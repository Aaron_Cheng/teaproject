@extends('layout.layout')

@section('title','首頁')

@section('content')
	<!-- 第一列 -->
	<section class="banner-area">
		<div class="container">
			<div class="row fullscreen align-items-center justify-content-between">
				<div class="col-lg-10 col-md-10 banner-left">
					<h6>發想</h6>
					<h1>將茶發揚光大</h1>
					<p>
						台灣茶產業是非常龐大的產業群，生產、加工、銷售、文化到相關行業，很多上市上櫃公司也都跨足茶產業，卻沒有專業的茶葉教學單位，農委會茶業改良場繁重的業務只能提供從事茶業相關人員實作學習；近期層出不窮的食安問題，可認知台灣最欠缺並且需要「食物教育」；講茶成立「講茶學院 TeaTalk Educational Agency （TTEA）」，採用完整系統的架構，讓學員能跟著「講茶學院」的課程，從初學到專業，更輕鬆的體驗台灣茶葉的廣度和深度；更可以協助茶產業各品牌做完整的專業人員訓練，讓員工擁有全面的茶專業知識和技能，成為該公司最強力的專業人員。
					</p>
					<a href="{{ url('/auth/google') }}" class="primary-btn text-uppercase">前往登入帳號</a>
				</div>
			</div>
		</div>					
	</section>
	<!-- 第一列結束 -->

	<!-- 第二列 -->
	<section class="home-about-area pt-120">
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<div class="col-lg-6 col-md-6 home-about-left">
					<img class="img-fluid" src="{{asset('img/tea.jpg')}}" alt="">
				</div>
				<div class="col-lg-5 col-md-6 home-about-right">
					<h6>仔細品嘗</h6>
					<h1 class="text-uppercase">茶的韻味</h1>
					<p>
						品過美麗茶湯、輕聞茶的飽滿茶香，令你難以忘懷嗎?
					</p>
					<a href="#" class="primary-btn text-uppercase">前往填寫表單</a>
				</div>
			</div>
			<br><br>
		</div>	
	</section>
	<!-- 第二列結束 -->

	<!-- 第三列 -->
	<section class="recent-blog-area section-gap">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8 pb-30 header-text">
					<h1>擁有自己的品評單</h1>
					<p>
						記錄下你所品嘗的每一種茶類
					</p>
				</div>
			</div>
			<div class="row">	
				<div class="single-recent-blog col-lg-4 col-md-4">
					<div class="thumb">
						<img class="f-img img-fluid mx-auto" src="{{asset('img/tea2.jpg')}}" alt="">	
					</div>
					<div class="bottom d-flex justify-content-between align-items-center flex-wrap"></div>							
					<a href="#">
						<h4>茶的口感</h4>
					</a>
					<p>
						茶入口的滑順感
					</p>
				</div>
				<div class="single-recent-blog col-lg-4 col-md-4">
					<div class="thumb">
						<img class="f-img img-fluid mx-auto" src="{{asset('img/tea3.jpg')}}" alt="">	
					</div>
					<div class="bottom d-flex justify-content-between align-items-center flex-wrap"></div>							
					<a href="#">
						<h4>茶的香氣</h4>
					</a>
					<p>
						茶沖泡出來的香氣
					</p>
				</div>
				<div class="single-recent-blog col-lg-4 col-md-4">
					<div class="thumb">
						<img class="f-img img-fluid mx-auto" src="{{asset('img/tea4.jpg')}}" alt="">	
					</div>
					<div class="bottom d-flex justify-content-between align-items-center flex-wrap"></div>							
					<a href="#">
						<h4>茶的味道</h4>
					</a>
					<p>
						茶回甘的滋味
					</p>
				</div>												
			</div>
		</div>	
	</section>
	<!-- 第三列結束 -->
@endsection

@section('css')
	<link rel="stylesheet" href="css/main.css">				
@endsection

@section('js')

@endsection