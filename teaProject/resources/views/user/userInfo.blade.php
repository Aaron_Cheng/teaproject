@extends('layout.layout')

@section('title','個人檔案')

@section('content')
        <section class="home-about-area pt-120">
            <div class="container">
                <div class="row align-items-center justify-content-between">
                    <div class="col-lg-6 col-md-6 home-about-left" style="margin-top: 20px;">
                        <img class="img-fluid" src="{{asset('img/logo1.png')}}" alt="">
                    </div>
                    <div class="col-lg-5 col-md-6 home-about-right">
                        <p>姓名 : {{ $user->userName }}</p>
                        <p>國籍 : {{ $user->nationality }}</p>
                        <p>性別 : {{ $user->sex }}</p>
                        <p>出生日期 : {{ $user->birth }}</p>
                        <p>教育程度 : {{ $user->education }}</p>
                        <p>信箱 : {{ $user->email }}</p>
                        <p>電話 : {{ $user->phone }}</p>
                        <p>居住地區 : {{ $user->residence }}</p>
                        <p>職業: {{ $user->profession }}</p>
                        <p>喝茶的頻率 : {{ $user->frequency }}</p>
                        <p>興趣 :
                            @foreach ( $user->hobbies as $hobby )
                                <li>{{{ $hobby }}}</li>
                            @endforeach
                        </p>
                        <p>茶香味的偏好 :
                            @foreach ( $user->preferenceFragrances as $preferenceFragrance )
                                <li>{{{ $preferenceFragrance }}}</li>
                            @endforeach
                        </p>
                        <p>喜歡的茶種 ：
                            @foreach ( $user->preferenceTeaTypes as $preferenceTeaType )
                                <li>{{{ $preferenceTeaType }}}</li>
                            @endforeach
                        </p>
                        <br><br>
                        <button class="btn primary-btn text-uppercase" onclick="window.location.href='/user-update-form'">前往修改</button>
                    </div>
                </div>
                <br><br>
            </div>
        </section>
@endsection

@section('css')
<link rel="stylesheet" href="css/main.css">
@endsection

@section('js')
@endsection
