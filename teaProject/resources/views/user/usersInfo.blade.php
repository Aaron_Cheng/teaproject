<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tea App</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div class="flex-center position-ref full-height" align="center">
            <h1>Users Info</h1>
            @foreach ($users as $user)
                <table border="1">
                    <tr>
                        <th>
                            Id
                        </th>
                        <td>
                            {{ $user->id }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Name
                        </th>
                        <td>
                            {{ $user->userName }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Nationality
                        </th>
                        <td>
                            {{ $user->nationality }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Sex
                        </th>
                        <td>
                            {{ $user->sex }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Birth
                        </th>
                        <td>
                            {{ $user->birth }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Education
                        </th>
                        <td>
                            {{ $user->education }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Email
                        </th>
                        <td>
                            {{ $user->email }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Phone
                        </th>
                        <td>
                            {{ $user->phone }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Residence
                        </th>
                        <td>
                            {{ $user->residence }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Profession
                        </th>
                        <td>
                            {{ $user->profession }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Frequency
                        </th>
                        <td>
                            {{ $user->frequency }}
                        </td>
                    </tr>
                </table><br/>
            @endforeach
        </div>
    </body>
</html>
