<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

use App\Models\BasicReport;

class BasicReportService
{
    private $url = "http://140.115.81.245:8080/";
    // private $url = "http://127.0.0.1:8080/";
    private $client;

    public function __construct(Client $client) {
        $this->client = $client;
    }

    public function findAll() {
        $response = $this->client->get($this->url.'basic-reports');
        $basicReportsJsonStr = $response->getBody()->getContents();
        $basicReportsJsonObj = json_decode($basicReportsJsonStr);
        $basicReports = collect();
        foreach ($basicReportsJsonObj as $basicReportJsonObj) {
            $basicReports->push($this->jsonObjToBasicReport($basicReportJsonObj));
        }
        return $basicReports;
    }

    public function findOne($basicReportId) {
        $response = $this->client->get($this->url.'basic-reports/'.$basicReportId);
        $basicReportJsonStr = $response->getBody()->getContents();
        $basicReportJsonObj = json_decode($basicReportJsonStr);
        $basicReport = $this->jsonObjToBasicReport($basicReportJsonObj);
        return $basicReport;
    }

    public function findByUserId($userId) {
        $response = $this->client->get($this->url.'users/'.$userId.'/basic-reports');
        $userBasicReportsJsonStr = $response->getBody()->getContents();
        $userBasicReportsJsonObj = json_decode($userBasicReportsJsonStr);
        $userBasicReports = collect();
        foreach ($userBasicReportsJsonObj as $userBasicReportJsonObj) {
            $userBasicReports->push($this->jsonObjToBasicReport($userBasicReportJsonObj));
        }
        return $userBasicReports;
    }

    public function save($userId, $basicReport) {
        $basicReportJsonStr = json_encode($basicReport);
        $response = $this->client->post($this->url.'users/'.$userId.'/basic-reports', ['body' => $basicReportJsonStr]);
        return $response->getBody()->getContents();
    }

    public function update($updateBasicReportId, $basicReport) {
        $basicReportJsonStr = json_encode($basicReport);
        $response = $this->client->put($this->url.'basic-reports/'.$updateBasicReportId, ['body' => $basicReportJsonStr]);
    }

    public function delete($deleteBasicReportId) {
        $response = $this->client->delete($this->url.'basic-reports/'.$deleteBasicReportId);
    }

    private function jsonObjToBasicReport($basicReportJsonObj) {
        $basicReport = new BasicReport();
        $basicReport->id = $basicReportJsonObj->id;
        $basicReport->productName = $basicReportJsonObj->productName;
        $basicReport->type = $basicReportJsonObj->type;
        $basicReport->origin = $basicReportJsonObj->origin;
        $basicReport->productionYear = $basicReportJsonObj->productionYear;
        $basicReport->varieties = $basicReportJsonObj->varieties;
        $basicReport->reportDate = $basicReportJsonObj->reportDate;
        $basicReport->aromas = $basicReportJsonObj->aromas;
        $basicReport->savors = $basicReportJsonObj->savors;
        $basicReport->tastes = $basicReportJsonObj->tastes;
        return $basicReport;
    }

}
