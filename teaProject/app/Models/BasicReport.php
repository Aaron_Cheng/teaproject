<?php

namespace App\Models;

class BasicReport
{
    public $id;
    public $productName;
    public $type;
    public $origin;
    public $productionYear;
    public $varieties;
    public $reportDate;
    public $aromas;
    public $savors;
    public $tastes;
}
