<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Project;
use App\Services\ProjectService;

class ProjectController extends Controller
{
    private $projectService;

    public function __construct(ProjectService $projectService) {
        $this->projectService = $projectService;
    }

    public function getProjects() {
        $projects = $this->projectService->findAll();
        return view('proejct.projectsInfo', ['projects' => $projects]);
    }

    public function getProject($projectId) {
        $project = $this->projectService->findOneForTeaTalk($projectId);
        return view('proejct.projectInfo', ['project' => $project]);
    }

    public function getProjectByProjectKey(Request $request) {
        $inputKey = $request->input('inputKey');
        $project = $this->projectService->findOneByProjectKey($inputKey);
        if($project !== null) {
            $request->session()->put('projectKey', $inputKey);
            return view('com-report.addComReportForm', ['project' => $project]);
        } else {
            print('驗證碼錯誤!');
        }
    }

    public function getMerchantProjects($merchantId) {
        $projects = $this->projectService->findByMerchantId($merchantId);
        return view('proejct.projectsInfo', ['projects' => $projects]);
    }

    public function getMerchantProject($merchantId, $projectId) {
        $project = $this->projectService->findOneForMerchant($merchantId, $projectId);
        return view('proejct.projectInfo', ['project' => $project]);
    }

    public function addProject(Request $request) {
        $merchantId = $request->input('merchantId');
        $project = new Project();
        $project->productName = $request->input('productName');
        $project->type = $request->input('type');
        $project->shape = $request->input('shape');
        $project->origin = $request->input('origin');
        $project->productionYear = $request->input('productionYear');
        $project->season = $request->input('season');
        $project->varieties = $request->input('varieties');
        $project->reportDate = $request->input('reportDate');
        $this->projectService->save($merchantId, $project);
    }

    public function updateProject(Request $request) {
        $updateProjectId = $request->input('updateProjectId');
        $project = new Project();
        $project->productName = $request->input('productName');
        $project->type = $request->input('type');
        $project->shape = $request->input('shape');
        $project->origin = $request->input('origin');
        $project->productionYear = $request->input('productionYear');
        $project->season = $request->input('season');
        $project->varieties = $request->input('varieties');
        $project->reportDate = $request->input('reportDate');
        $this->projectService->update($updateProjectId, $project);
    }

    public function deleteProject(Request $request) {
        $deleteProjectId = $request->input('deleteProjectId');
        $this->projectService->delete($deleteProjectId);
    }

}
