@extends('layouts.app')

@section('title', '課程查詢')

@section('content')
	<div class="container">
		<!--標題-->
		<div class="page-header">
			<center>課程查詢</center>
		</div>
		<br>
		<div class="wrapper row2">
  <section class="hoc container clear">
    <ul class="nospace group services" >
      <li class="one_quarter first" data-toggle="tooltip" data-placement="bottom" title=" 不足16小時之單一課程!">
        <article><a href="{{ url('/record/個別課程') }}"><img src="/img/as.jpg" alt="" width="300"></a>
          <h3 class="heading font-x1"><a href="{{ url('/record/個別課程') }}">個別課程</a></h3>
        </article>
      </li>
      <li class="one_quarter" data-toggle="tooltip" data-placement="bottom" title="滿足1學分16小時或2學分32小時之套裝課程!">
        <article><a href="{{ url('/record/套裝課程') }}"><img src="/img/as2.jpg" alt="" width="300"></a>
          <h6 class="heading font-x1"><a href="{{ url('/record/套裝課程') }}">套裝課程</a></h6>
        </article>
      </li>
      <li class="one_quarter">
        <article><a href="{{ url('/search') }}">
          <h6 class="heading font-x1"><img src="/img/as21.jpg" alt="" width="300"><a href="{{ url('/search') }}">詳細搜尋</a></h6>
        </article>
      </li>
    </ul>
  </section>
</div>

	 </div>

@endsection

@section('css')
<link rel="stylesheet" href="/css/search.css">
<style type="text/css">
	body{
		font-family: 微軟正黑體;
	}
	nav{
		font-size: large; !important;
		font-family:"Microsoft JhengHei"; !important
	}

		.page-header{
			font-size: 35px;
			margin-top: 100px;
			/*font-weight: bold;*/
		}
		/*查詢歷年課程字體*/
		font{
			font-size: 20px;
		}

		/*內容*/
		.content{
			margin-left: 4%;
		}

		/*Modal標題字體*/
		h4{
			font-weight: bold;
		}
</style>
@endsection

@section('js')
<script>
 $(document).ready(function(){
		 $('[data-toggle="tooltip"]').tooltip();
 });
 </script>
@endsection
