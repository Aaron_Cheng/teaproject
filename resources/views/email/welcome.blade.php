<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>您好，此封郵件由微學分平台信箱自動寄出，請勿回信</h2>

<div>

    我們已將您的微學分密碼重置，目前您的密碼會與您的帳號一致。
    請盡速以此密碼登入，並提醒您修改密碼。
    微學分平台感謝您的使用。
</div>

</body>
</html>
