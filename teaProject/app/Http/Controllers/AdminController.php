<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function getAdminHomePage(Request $request) {
        $user = $request->session()->get('user');
        if($user === 'admin') {
            return view('authority.index');
        } else {
            return view('error.page-not-found');
        }
    }
}
