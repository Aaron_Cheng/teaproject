@extends('layout.layout')

@section('title','會員資料註冊')

@section('content')
    <section class="home-about-area pt-120">
            <div class="container">
                <div class="row align-items-center justify-content-between">
                    <div class="col-lg-6 col-md-6 home-about-left" style="margin-top: 20px;">
                        <img class="img-fluid" src="{{asset('img/logo1.png')}}" alt="">
                    </div>
                    <div class="col-lg-5 col-md-6 home-about-right">
                        <form action="/user-register" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="wrap-input100 validate-input m-b-26" data-validate="不能為空白">
                                <span class="label-input100">姓名：</span>
                                    {{ $googleUser->name }}
                                <span class="focus-input100"></span>
                            </div>
                            <div class="wrap-input100 validate-input m-b-26" data-validate="不能為空白">
                                <span class="label-input100">信箱：</span>
                                    {{ $googleUser->email }}
                                <span class="focus-input100"></span>
                            </div>
                            <div class="wrap-input100 validate-input m-b-26" data-validate="不能為空白">
                                <span class="label-input100">國籍：</span>
                                    <input class="input100" type="text" name="nationality">
                                <span class="focus-input100"></span>
                            </div>
                            <div class=" validate-input m-b-26" data-validate="不能為空白">
                                <span class="label-input100">性別：</span>
                                    <input type="radio" name="sex" value="M" /> Male
                                    <input type="radio" name="sex" value="F" /> Female
                                    <input type="radio" name="sex" value="O" /> Other<br/><br/>
                                <span class="focus-input100"></span>
                            </div>
                            <div class="wrap-input100 validate-input m-b-26" data-validate="不能為空白">
                                <span class="label-input100">出生日期：</span>
                                    <input class="input100" type="text" name="birth">
                                <span class="focus-input100"></span>
                            </div>
                            <div class="wrap-input100 validate-input m-b-26" data-validate="不能為空白">
                                <span class="label-input100">教育程度：</span>
                                    <input class="input100" type="text" name="education">
                                <span class="focus-input100"></span>
                            </div>   
                            <div class="wrap-input100 validate-input m-b-26" data-validate="不能為空白">
                                <span class="label-input100">電話：</span>
                                    <input class="input100" type="text" name="phone">
                                <span class="focus-input100"></span>
                            </div>
                            <div class="wrap-input100 validate-input m-b-26" data-validate="不能為空白">
                                <span class="label-input100">居住地區：</span>
                                    <input class="input100" type="text" name="residence">
                                <span class="focus-input100"></span>
                            </div>
                            <div class="wrap-input100 validate-input m-b-26" data-validate="不能為空白">
                                <span class="label-input100">職業：</span>
                                    <input class="input100" type="text" name="profession">
                                <span class="focus-input100"></span>
                            </div> 
                            <div class="wrap-input100 validate-input m-b-26" data-validate="不能為空白">
                                <span class="label-input100">喝茶的頻率：</span>
                                    <input class="input100" type="text" name="frequency">
                                <span class="focus-input100"></span>
                            </div>
                            <div class="validate-input m-b-26" data-validate="不能為空白">
                                <span class="label-input100">興趣：</span>
                                    <input type="checkbox" name="hobbies[]" value="arts"> arts
                                    <input type="checkbox" name="hobbies[]" value="cuisine"> cuisine
                                    <input type="checkbox" name="hobbies[]" value="exercise"> exercise
                                    <input type="checkbox" name="hobbies[]" value="travel"> travel
                                    <input type="checkbox" name="hobbies[]" value="technology"> technology
                                    <input type="checkbox" name="hobbies[]" value="entertainment"> entertainment<br/><br/>
                                    <input type="checkbox" name="hobbies[]" value="learning"> learning
                                    <input type="checkbox" name="hobbies[]" value="fashion"> fashion
                                    <input type="checkbox" name="hobbies[]" value="charity"> charity
                                    <input type="checkbox" name="hobbies[]" value="photography"> photography
                                    <input type="checkbox" name="hobbies[]" value="commerce"> commerce
                                    <input type="checkbox" name="hobbies[]" value="health"> health<br/><br/><br/>
                                <span class="focus-input100"></span>
                            </div>
                            <div class="validate-input m-b-26" data-validate="不能為空白">
                                <span class="label-input100">茶香味的偏好：</span>
                                    <input type="checkbox" name="preferenceFragrances[]" value="herbal scent"> herbal scent
                                    <input type="checkbox" name="preferenceFragrances[]" value="floral"> floral
                                    <input type="checkbox" name="preferenceFragrances[]" value="cereal"> cereal
                                    <input type="checkbox" name="preferenceFragrances[]" value="sweet candy"> sweet candy
                                    <input type="checkbox" name="preferenceFragrances[]" value="fruit aroma"> fruit aroma<br/><br/>
                                    <input type="checkbox" name="preferenceFragrances[]" value="aour and fermented"> aour and fermented
                                    <input type="checkbox" name="preferenceFragrances[]" value="wood grain"> wood grain
                                    <input type="checkbox" name="preferenceFragrances[]" value="spice flavor"> spice flavor
                                    <input type="checkbox" name="preferenceFragrances[]" value="cocoa flavor"> cocoa flavor
                                    <input type="checkbox" name="preferenceFragrances[]" value="baking"> baking<br/><br/><br/>
                                <span class="focus-input100"></span>
                            </div>
                            <div class="validate-input m-b-26" data-validate="不能為空白">
                                <span class="label-input100">喜歡的茶種：</span>
                                    <input type="checkbox" name="preferenceTeaTypes[]" value="alpine oolong"> alpine oolong
                                    <input type="checkbox" name="preferenceTeaTypes[]" value="wenshan baozong tea"> wenshan baozong tea
                                    <input type="checkbox" name="preferenceTeaTypes[]" value="tie guanyin tea"> tie guanyin tea
                                    <input type="checkbox" name="preferenceTeaTypes[]" value="foreign brand black tea"> foreign brand black tea
                                    <input type="checkbox" name="preferenceTeaTypes[]" value="taiwan black tea"> taiwan black tea<br/><br/>
                                    <input type="checkbox" name="preferenceTeaTypes[]" value="oriental beauty tea"> oriental beauty tea
                                    <input type="checkbox" name="preferenceTeaTypes[]" value="honey series"> honey series
                                    <input type="checkbox" name="preferenceTeaTypes[]" value="pu'er tea"> pu'er tea
                                    <input type="checkbox" name="preferenceTeaTypes[]" value="green tea"> green tea
                                    <input type="checkbox" name="preferenceTeaTypes[]" value="japanese matcha"> japanese matcha<br/><br/><br/>
                                <span class="focus-input100"></span>
                            </div>                        
                            <input type="submit" value="Submit" /><br/><br/>
                        </form>
                    </div>
                </div>
            </div>
    </section>
@endsection

@section('css')
    <link rel="stylesheet" href="css/main.css">             
@endsection

@section('js')

@endsection