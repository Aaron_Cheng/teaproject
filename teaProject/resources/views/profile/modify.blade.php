@extends('layout.layout')

@section('title','修改個人檔案')

@section('content')
	<!-- Start home-about Area -->
			<section class="home-about-area pt-120">
				<div class="container">
					<div class="row align-items-center justify-content-between">
						<div class="col-lg-6 col-md-6 home-about-left">
							<img class="img-fluid" src="{{asset('img/logo1.png')}}" alt="">
						</div>
						<div class="col-lg-5 col-md-6 home-about-right">
							<div class="input">
								<p style="display: inline;">姓名 :</p>
								<input type="" name="" placeholder="王小明">
							</div>
							<div class="input">
								<p style="display: inline;">國籍 :</p>
								<input type="" name="" placeholder="台灣">
							</div>
							<div class="input">
								<p style="display: inline;">性別 :</p>
								<input type="" name="" placeholder="男">
							</div>
							<div class="input">
								<p style="display: inline;">出生日期 :</p>
								<input type="" name="" placeholder="2000/02/02">
							</div>
							<div class="input">
								<p style="display: inline;">教育程度 :</p>
								<input type="" name="" placeholder="大專院校">
							</div>
							<div class="input">
								<p style="display: inline;">信箱 :</p>
								<input type="" name="" placeholder="123456789@gmail.com">
							</div>
							<div class="input">
								<p style="display: inline;">電話 :</p>
								<input type="" name="" placeholder="0912345678">
							</div>
						</div>
					</div>
					<br><br>
				</div>	
			</section>
@endsection

@section('css')
<link rel="stylesheet" href="css/main.css">
<style type="text/css">
	.input{
		margin-bottom: 20px;
	}
</style>
@endsection

@section('js')
@endsection