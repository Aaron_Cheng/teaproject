@extends('layout.layout')

@section('title','修改個人檔案')

@section('content')
    <form action="/project-check-key" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <section class="home-about-area pt-120">
            <div class="container">

                <div class="row align-items-center justify-content-between">
                    <div class="col-lg-6 col-md-6 home-about-left">
                        <img class="img-fluid" src="{{asset('img/logo1.png')}}" alt="">
                    </div>
                    <div class="col-lg-6 col-md-6 home-about-right">
                        <div class="wrap-input100 validate-input m-b-26" data-validate="不能為空白">
                                <span class="label-input100">商業版驗證碼：</span>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input class="input100" type="text" name="inputKey">
                                <span class="focus-input100"></span>
                        </div>
                        <br>
                        <center>
                            <button type="submit" value="Submit" class="btn primary-btn text-uppercase">送出驗證碼</button>
                        </center>
                        


<!--                         <br><br><br>ZKDPL68ST3
                        <br>6RRFPHZ3ZK -->
                    </div>
                </div>
                <br><br>
            </div>
        </section>
    </form>

@endsection

@section('css')
    <link rel="stylesheet" href="css/main.css">
    <!-- 表單input的CSS -->
    <link rel="stylesheet" href="css/form/form.css">
    <style type="text/css">
        .checkbox-inline{
            margin-right: 5%;
        }

        .checkbox-left{
            margin-left: 3%;
        }

        .tatse-title{
            margin-right: 5%;
        }

        .radio-inline{
            margin-right: 5%;
        }
    </style>
@endsection

@section('js')
    <!-- pickadate.js v3.5.6 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/themes/classic.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/themes/classic.date.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/themes/classic.time.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.date.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.time.js"></script>
    <script>
        $( ".example" ).pickadate();
    </script>
@endsection
