<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

use App\Models\ProReport;

class ProReportService
{
    private $url = "http://140.115.81.245:8080/";
    // private $url = "http://127.0.0.1:8080/";
    private $client;

    public function __construct(Client $client) {
        $this->client = $client;
    }

    public function findAll() {
        $response = $this->client->get($this->url.'pro-reports');
        $proReportsJsonStr = $response->getBody()->getContents();
        $proReportsJsonObj = json_decode($proReportsJsonStr);
        $proReports = collect();
        foreach ($proReportsJsonObj as $proReportJsonObj) {
            $proReports->push($this->jsonObjToProReport($proReportJsonObj));
        }
        return $proReports;
    }

    public function findOne($proReportId) {
        $response = $this->client->get($this->url.'pro-reports/'.$proReportId);
        $proReportJsonStr = $response->getBody()->getContents();
        $proReportJsonObj = json_decode($proReportJsonStr);
        $proReport = $this->jsonObjToProReport($proReportJsonObj);
        return $proReport;
    }

    public function findByUserId($userId) {
        $response = $this->client->get($this->url.'users/'.$userId.'/pro-reports');
        $userProReportsJsonStr = $response->getBody()->getContents();
        $userProReportsJsonObj = json_decode($userProReportsJsonStr);
        $userProReports = collect();
        foreach ($userProReportsJsonObj as $userProReportJsonObj) {
            $userProReports->push($this->jsonObjToProReport($userProReportJsonObj));
        }
        return $userProReports;
    }

    public function save($userId, $proReport) {
        $proReportJsonStr = json_encode($proReport);
        $response = $this->client->post($this->url.'users/'.$userId.'/pro-reports', ['body' => $proReportJsonStr]);
        return $response->getBody()->getContents();
    }

    public function update($updateProReportId, $proReport) {
        $proReportJsonStr = json_encode($proReport);
        $response = $this->client->put($this->url.'pro-reports/'.$updateProReportId, ['body' => $proReportJsonStr]);
    }

    public function delete($deleteProReportId) {
        $response = $this->client->delete($this->url.'pro-reports/'.$deleteProReportId);
    }

    private function jsonObjToProReport($proReportJsonObj) {
        $proReport = new ProReport();
        $proReport->id = $proReportJsonObj->id;
        $proReport->productName = $proReportJsonObj->productName;
        $proReport->type = $proReportJsonObj->type;
        $proReport->shape = $proReportJsonObj->shape;
        $proReport->origin = $proReportJsonObj->origin;
        $proReport->productionYear = $proReportJsonObj->productionYear;
        $proReport->season = $proReportJsonObj->season;
        $proReport->varieties = $proReportJsonObj->varieties;
        $proReport->reportDate = $proReportJsonObj->reportDate;
        $proReport->aromas = $proReportJsonObj->aromas;
        $proReport->savors = $proReportJsonObj->savors;
        $proReport->tastes = $proReportJsonObj->tastes;
        return $proReport;
    }

}
