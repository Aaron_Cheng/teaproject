<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\UserService;

use Socialite;

class GoogleController extends Controller
{
    private $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    public function redirectToProvider()
    {
        $user = session('user');
        if($user === null) {
            $googleUser = session('googleUser');
            if($googleUser === null) {
                return Socialite::driver('google')->redirect();
            } else {
                print('已登入 Google 帳號但尚未完成註冊~');
            }
        } else {
            print('已登入~');
        }
    }


    public function handleProviderCallback()
    {
        $googleUser = Socialite::driver('google')->stateless()->user();
        $user = $this->userService->findByEmail($googleUser->email);
        if($user !== null) {
            if($user === 'admin') {
                session(['user' => 'admin']);
                return redirect()->action('AdminController@getAdminHomePage');
            } else {
                session(['user' => $user]);
                return redirect()->action('UserController@getInfoForUser');
            }
        } else {
            session(['googleUser' => $googleUser]);
            return view('user.userRegisterForm', ['googleUser' => $googleUser]);
        }
    }

}
