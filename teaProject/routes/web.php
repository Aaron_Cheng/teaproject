<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage.homepage');
});

// 首頁
Route::get('homepage', function () {
    return view('homepage.homepage');
});

// 個人資料
Route::get('profile', function () {
    return view('profile.index');
});
Route::get('profile_modify', function () {
    return view('profile.modify');
});
Route::get('profile_unlogin', function () {
    return view('profile.unlogin');
});
Route::get('/certificate-edit', function () {
    return view('certificate.edit');
});



// 品評單專區
// Route::get('certificate', function () {
//     return view('certificate.index');
// });

// 品評單
Route::get('form_normal', function () {
    return view('form.form_normal');
});
Route::get('form_professional', function () {
    return view('form.form_professional');
});
Route::get('form_business', function () {
    return view('form.form_business');
});
Route::get('form_business/vertification', function () {
    return view('form.vertification');
});
Route::get('form_successful', function () {
    return view('form.successful');
});
Route::get('form_AlreadyAdd', function () {
    return view('form.AlreadyAdd');
});

// 帳號登入
Route::get('login', function () {
    return view('login.index');
});

// 後台管理
// Route::get('authority', function () {
//     return view('authority.index');
// });

#-------------------------------------------


# Login & Logout {
    Route::get('/auth/google', 'Auth\GoogleController@redirectToProvider');
    Route::get('/auth/google/callback', 'Auth\GoogleController@handleProviderCallback');

    Route::get('/logout', 'UserController@userLogout');
# }


# User {
    // 使用者主頁
    Route::get('/user-info', 'UserController@getInfoForUser');

    // 使用者註冊
    Route::post('/user-register', 'UserController@addUser');

    // 使用者更新資料
    Route::get('/user-update-form', 'UserController@returnUserUpdateForm');
    Route::post('/user-update', 'UserController@updateUser');

    // 刪除使用者
    Route::get('/user-delete-form', function() {
        return view('user.userDeleteForm');
    });
    Route::post('/user-delete', 'UserController@deleteUser');
# }


# Admin {
    Route::get('/teatalk-authority-homepage','AdminController@getAdminHomePage');
    // 樞紐分析表
    Route::get('/teatalk-authority-pivottable', function() {
        return view('authority.pivot');
    });
# }


# Basic Report {
    // 基礎品評單主頁
    Route::get('/user/basic-reports', 'BasicReportController@getUserBasicReports');

    // 查看單一基礎品評單
    Route::get('/basic-reports/{basicReportId}', 'BasicReportController@getBasicReport');

    // 新增基礎品評單
    Route::get('/add-basic-report-form', 'BasicReportController@getAddBasicReportForm');
    Route::post('/basic-report-add', 'BasicReportController@addBasicReport');

    Route::post('/basic-report-update', 'BasicReportController@updateBasicReport');
    Route::post('/basic-report-delete', 'BasicReportController@deleteBasicReport');
# }


# Pro Report {
    // 專業品評單主頁
    Route::get('/user/pro-reports', 'ProReportController@getUserProReports');

    // 查看單一專業品評單
    Route::get('/pro-reports/{proReportId}', 'ProReportController@getProReport');

    // 新增專業品評單
    Route::get('/add-pro-report-form', 'ProReportController@getAddProReportForm');
    Route::post('/pro-report-add', 'ProReportController@addProReport');

    Route::post('/pro-report-update', 'ProReportController@updateProReport');
    Route::post('/pro-report-delete', 'ProReportController@deleteProReport');
# }



# Com Report {
    // 商業品評單主頁
    Route::get('/user/com-reports', 'ComReportController@getUserComReports');

    // 查看單一商業品評單
    Route::get('/com-reports/{comReportId}', 'ComReportController@getComReport');

    // 檢查商業品評單驗證碼
    Route::get('/check-project-key-form', 'ComReportController@getAddComReportForm');
    Route::post('/project-check-key', 'ProjectController@getProjectByProjectKey');

    // 新增商業品評單
    Route::post('/com-report-add', 'ComReportController@addComReport');

    Route::post('/com-report-update', 'ComReportController@updateComReport');
    Route::post('/com-report-delete', 'ComReportController@deleteComReport');
# }



# Certificate {
    Route::get('/certificate', 'ShowReportsController@getAllReports');
    Route::post('/certificate/basic-report-pdf', 'ShowReportsController@getBasicReportPdf');
    Route::post('/certificate/pro-report-pdf', 'ShowReportsController@getProReportPdf');
    Route::post('/certificate/com-report-pdf', 'ShowReportsController@getComReportPdf');
# }


# tmp {
    Route::get('/users', 'UserController@getUsers');
# }

