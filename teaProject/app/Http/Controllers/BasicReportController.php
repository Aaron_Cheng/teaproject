<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\BasicReport;
use App\Services\BasicReportService;

class BasicReportController extends Controller
{
    private $basicReportService;

    public function __construct(BasicReportService $basicReportService) {
        $this->basicReportService = $basicReportService;
    }

    public function getBasicReports() {
        $basicReports = $this->basicReportService->findAll();
        return view('basic-report.basicReportsInfo', ['basicReports' => $basicReports]);
    }

    public function getBasicReport($basicReportId) {
        $basicReport = $this->basicReportService->findOne($basicReportId);
        return view('basic-report.basicReportInfo', ['basicReport' => $basicReport]);
    }

    public function getUserBasicReports() {
        $userId = session('user')->id;
        $userBasicReports = $this->basicReportService->findByUserId($userId);
        return view('basic-report.basicReportsInfo', ['basicReports' => $userBasicReports]);
    }

    public function getAddBasicReportForm(Request $request) {
        $user = $request->session()->get('user');
        if($user !== null) {
            return view('basic-report.addBasicReportForm');
        } else {
            return view('login.unlogin');
        }
    }

    public function addBasicReport(Request $request) {
        $userId = $request->session()->get('user')->id;
        $basicReport = new BasicReport();
        $basicReport->productName = $request->input('productName');
        $basicReport->type = $request->input('type');
        $basicReport->origin = $request->input('origin');
        $basicReport->productionYear = $request->input('productionYear');
        $basicReport->varieties = $request->input('varieties');
        $basicReport->reportDate = $request->input('reportDate');
        $basicReport->aromas = $request->input('aromas');
        $savors = [];
        $savors[0] = $request->input('savor_sour');
        $savors[1] = $request->input('savor_sweet');
        $savors[2] = $request->input('savor_bitter');
        $savors[3] = $request->input('savor_salty');
        $savors[4] = $request->input('savor_fresh');
        $savors[5] = $request->input('savor_astringent');
        $savors[6] = $request->input('savor_spicy');
        $basicReport->savors = $savors;
        $basicReport->tastes = $request->input('tastes');
        $result = $this->basicReportService->save($userId, $basicReport);
        if($result === 'success') {
            $request->session()->put('userBasicReports', $this->basicReportService->findByUserId($userId));
            return redirect()->action('ShowReportsController@getAllReports');
        } elseif ($result === 'fail') {
            print('新增失敗，請檢查所輸入的資料~');
        } else {
            print('新增失敗，請檢查所輸入的資料~');
            print('</br>');
            print('Detail: ' . $result);
        }
    }

    public function updateBasicReport(Request $request) {
        $updateBasicReportId = $request->input('updateBasicReportId');
        $basicReport = new BasicReport();
        $basicReport->productName = $request->input('productName');
        $basicReport->type = $request->input('type');
        $basicReport->origin = $request->input('origin');
        $basicReport->productionYear = $request->input('productionYear');
        $basicReport->varieties = $request->input('varieties');
        $basicReport->reportDate = $request->input('reportDate');
        $basicReport->aromas = $request->input('aromas');
        $basicReport->savors = $request->input('savors');
        $basicReport->tastes = $request->input('tastes');
        $this->basicReportService->update($updateBasicReportId, $basicReport);
    }

    public function deleteBasicReport(Request $request) {
        $deleteBasicReportId = $request->input('deleteBasicReportId');
        $this->basicReportService->delete($deleteBasicReportId);
    }

}
