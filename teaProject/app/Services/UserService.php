<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

use App\Models\User;

class UserService
{
    private $url = "http://140.115.81.245:8080/users/";
    // private $url = "http://127.0.0.1:8080/users/";
    private $client;

    public function __construct(Client $client) {
        $this->client = $client;
    }

    public function findAll() {
        $response = $this->client->get($this->url);
        $usersJsonStr = $response->getBody()->getContents();
        $usersJsonObj = json_decode($usersJsonStr);
        $users = collect();
        foreach ($usersJsonObj as $userJsonObj) {
            $users->push($this->jsonObjToUser($userJsonObj));
        }
        return $users;
    }

    public function findOne($userId) {
        $response = $this->client->get($this->url.$userId);
        $userJsonStr = $response->getBody()->getContents();
        if($userJsonStr !== 'null') {
            $userJsonObj = json_decode($userJsonStr);
            $user = $this->jsonObjToUser($userJsonObj);
            return $user;
        } else {
            return null;
        }
    }

    public function findByEmail($userEmail) {
        $response = $this->client->post($this->url.'email', ['body' => $userEmail]);
        $userJsonStr = $response->getBody()->getContents();
        if ($userJsonStr === 'admin') {
            return 'admin';
        } elseif ($userJsonStr !== 'null') {
            $userJsonObj = json_decode($userJsonStr);
            $user = $this->jsonObjToUser($userJsonObj);
            return $user;
        } else {
            return null;
        }
    }

    public function save($user) {
        $userJsonStr = json_encode($user);
        $response = $this->client->post($this->url, ['body' => $userJsonStr]);
        $userJsonStr = $response->getBody()->getContents();
        $userJsonObj = json_decode($userJsonStr);
        $user = $this->jsonObjToUser($userJsonObj);
        return $user;
    }

    public function update($updateUserId, $user) {
        $userJsonStr = json_encode($user);
        $response = $this->client->put($this->url.$updateUserId, ['body' => $userJsonStr]);
        $updatedUserJsonStr = $response->getBody()->getContents();
        $updatedUserJsonObj = json_decode($updatedUserJsonStr);
        $updatedUser = $this->jsonObjToUser($updatedUserJsonObj);
        return $updatedUser;
    }

    public function delete($deleteUserId) {
        $response = $this->client->delete($this->url.$deleteUserId);
    }

    private function jsonObjToUser($userJsonObj) {
        $user = new User();
        $user->id = $userJsonObj->id;
        $user->userName = $userJsonObj->userName;
        $user->nationality = $userJsonObj->nationality;
        $user->sex = $userJsonObj->sex;
        $user->birth = $userJsonObj->birth;
        $user->education = $userJsonObj->education;
        $user->email = $userJsonObj->email;
        $user->phone = $userJsonObj->phone;
        $user->residence = $userJsonObj->residence;
        $user->profession = $userJsonObj->profession;
        $user->frequency = $userJsonObj->frequency;
        $user->hobbies = $userJsonObj->hobbies;
        $user->preferenceFragrances = $userJsonObj->preferenceFragrances;
        $user->preferenceTeaTypes = $userJsonObj->preferenceTeaTypes;
        return $user;
    }

}
