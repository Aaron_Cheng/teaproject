<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use App\Photo;

class PhotoController extends Controller
{
    public function index(){
      $photo=Photo::all();
      return view('authrize/photo.index',["photo"=>$photo]);
    }
    public function create(){
      return view('authrize/photo.create');
    }
    public function store(Request $request){
      $this->validate($request, [
          'title' => 'required',
          'subtitle' => 'required',
          'photo' => 'required',
          'color'=>'required',
      ],[
        'title.required'=>'請填寫公告標題',
        'subtitle.required'=>'請填寫公告小標',
        'photo.required'=>'請上傳圖片',
        'color.required'=>'請選擇字體顏色',
        ]);

        $photo=new Photo;
        $photo->title=$request->title;
        $photo->subtitle=$request->subtitle;
        $photo->color=$request->color;

        $name=pathinfo($request->photo->getClientOriginalName(), PATHINFO_FILENAME);
        $ext=pathinfo($request->photo->getClientOriginalName(), PATHINFO_EXTENSION);
        $fname=$name.'.'.$ext;
        $request->photo->move(public_path('/photo'), $fname);
        $photo->path=$fname;
        $photo->save();

      return redirect('authrize/menu/photo');
    }
    public function edit($id){
    //
    }
    public function update(Request $request,$id){
      $this->validate($request, [
          'title' => 'required',
          'subtitle' => 'required',
          'color'=>'required',
      ],[
        'title.required'=>'請填寫公告標題',
        'subtitle.required'=>'請填寫公告小標',
        'color.required'=>'請選擇文字顏色',
        ]);
      $photo=Photo::find($id);
      $photo->title=$request->title;
      $photo->subtitle=$request->subtitle;
      $photo->color=$request->color;
      $photo->save();
      return redirect('authrize/menu/photo');

    }
    public function destroy($id){
      $photo=Photo::find($id);
      $title=$photo->title;
      $image_path = public_path().'/photo/'.$photo->path;
       if(File::exists($image_path)) {
         File::delete($image_path);
       }
      Photo::destroy($id);
      return back()->with('success','success')->with('title',$title);
    }
}
