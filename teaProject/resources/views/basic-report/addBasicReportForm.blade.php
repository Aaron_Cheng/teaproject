@extends('layout.layout')

@section('title','初階版品評單')

@section('content')
        <section class="home-about-area pt-120">
            <div class="container">
                <div class="row fullscreen align-items-center justify-content-between">
                    <div class="col-lg-6 col-md-6 banner-left" style="margin-top: 20%;">
                        <form action="/basic-report-add" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <!-- 標題 -->
                            <h3><center>初階版品評單</center></h3><br><br><br>

                            <!-- 品名 -->
                            <div class="wrap-input100 validate-input m-b-26">
                                <span class="label-input100">品名：</span>
                                    <input class="input100" type="text" name="productName" placeholder="請輸入品名">
                                <span class="focus-input100"></span>
                            </div>
                            <br>
                            <!-- 茶類 -->
                            <div class="validate-input m-b-26">
                                <span class="label-input100">茶類：</span>
                                    <select class="form-control" name="type">
                                        <option value="" selected disabled>請選擇一種茶類
                                            <span class="caret"></span>
                                        </option>
                                        <option value="black">紅茶</option>
                                        <option value="green">綠茶</option>
                                        <option value="oolong">烏龍茶</option>
                                        <option value="yellow">黃茶</option>
                                        <option value="white">白茶</option>
                                        <option value="dark">黑茶</option>
                                    </select>
                                <span class="focus-input100"></span>
                            </div>
                            <!-- 產區 -->
                            <div class="wrap-input100 validate-input m-b-26">
                                <span class="label-input100">產區：</span>
                                    <input class="input100" type="text" name="origin" placeholder="請輸入產區">
                                <span class="focus-input100"></span>
                            </div>

                            <!-- 年份 -->
                            <div class="wrap-input100 validate-input m-b-26">
                                <span class="label-input100">年份：</span>
                                    <input class="input100" type="text" name="productionYear" placeholder="請輸入年份">
                                <span class="focus-input100"></span>
                            </div>

                            <!-- 品種 -->
                            <div class="wrap-input100 validate-input m-b-26">
                                <span class="label-input100">品種：</span>
                                    <input class="input100" type="text" name="varieties" placeholder="請輸入品種">
                                <span class="focus-input100"></span>
                            </div>

                            <!-- 品茶日期 -->
                            <div class="wrap-input100 validate-input m-b-26">
                                <span class="label-input100">品茶日期：</span>
                                    <input class="input100" type="text" name="reportDate" placeholder="請輸入茶品名稱">
                                <span class="focus-input100"></span>
                            </div>

                            <!-- 香氣 -->
                            <div class="validate-input m-b-26">
                                <span class="label-input100">香氣：</span>
                                    <select id="ms" multiple="multiple" name="aromas[]">
                                        <option value="fresh grass">青草香</option>
                                        <option value="soybean">豆香</option>
                                        <option value="seaweed">海苔</option>
                                        <option value="mint">薄荷</option>
                                        <option value="floral">花香</option>
                                        <option value="unripe fruit">青澀果香</option>
                                        <option value="citrix">柑橘</option>
                                        <option value="plum">梅子</option>
                                        <option value="peach">桃子</option>
                                        <option value="red plum">紅色梅果</option>
                                        <option value="ripe fruit">成熟果香</option>
                                        <option value="long an">桂圓</option>
                                        <option value="nut">堅果</option>
                                        <option value="malt">麥芽</option>
                                        <option value="wood grain">木質香</option>
                                        <option value="soil">泥土</option>
                                        <option value="milk">奶香</option>
                                        <option value="honey">蜜香</option>
                                        <option value="sugar">糖香</option>
                                        <option value="fried rice">炒米香</option>
                                        <option value="fire incense">火香</option>
                                        <option value="charcoal">炭香</option>
                                        <option value="burning">焦味</option>
                                    </select>
                                    <!-- <br/>
                                    <input type="checkbox" name="aromas[]" value="fresh grass"> 青草香
                                    <input type="checkbox" name="aromas[]" value="soybean"> 豆香
                                    <input type="checkbox" name="aromas[]" value="seaweed"> 海苔
                                    <input type="checkbox" name="aromas[]" value="mint"> 薄荷<br/><br/>
                                    <input type="checkbox" name="aromas[]" value="floral"> 花香
                                    <input type="checkbox" name="aromas[]" value="unripe fruit"> 青澀果香
                                    <input type="checkbox" name="aromas[]" value="citrix"> 柑橘
                                    <input type="checkbox" name="aromas[]" value="plum"> 梅子<br/><br/>
                                    <input type="checkbox" name="aromas[]" value="red plum"> 紅色梅果
                                    <input type="checkbox" name="aromas[]" value="ripe fruit"> 成熟果香
                                    <input type="checkbox" name="aromas[]" value="long an"> 桂圓
                                    <input type="checkbox" name="aromas[]" value="nut"> 堅果<br/><br/>
                                    <input type="checkbox" name="aromas[]" value="malt"> 麥芽
                                    <input type="checkbox" name="aromas[]" value="wood grain"> 木質香
                                    <input type="checkbox" name="aromas[]" value="soil"> 泥土
                                    <input type="checkbox" name="aromas[]" value="milk"> 奶香<br/><br/>
                                    <input type="checkbox" name="aromas[]" value="honey"> 蜜香
                                    <input type="checkbox" name="aromas[]" value="sugar"> 糖香
                                    <input type="checkbox" name="aromas[]" value="fried rice"> 炒米香
                                    <input type="checkbox" name="aromas[]" value="fire incense"> 火香<br/><br/>
                                    <input type="checkbox" name="aromas[]" value="charcoal"> 炭香
                                    <input type="checkbox" name="aromas[]" value="burning"> 焦味<br/><br/><br/><br/> -->
                                <span class="focus-input100"></span>
                            </div>

                            <!-- 滋味 -->
                            <div class="validate-input m-b-26">
                                <span class="label-input100">滋味：</span>
                                    <br><br>

                                    <label>酸味</label>
                                    <select class="form-control" name="savor_sour">
                                        <option value="" selected disabled>請選擇酸味程度
                                            <span class="caret"></span>
                                        </option>
                                        <option value="barely sour">無酸味</option>
                                        <option value="slightly sour">微酸味</option>
                                        <option value="medium sour">中度酸味</option>
                                        <option value="very sour">高度酸味</option>
                                    </select>

                                    <label>甜味</label>
                                    <select class="form-control" name="savor_sweet">
                                        <option value="" selected disabled>請選擇甜味程度
                                            <span class="caret"></span>
                                        </option>
                                        <option value="barely sweet">無甜味</option>
                                        <option value="slightly sweet">微甜味</option>
                                        <option value="medium sweet">中度甜味</option>
                                        <option value="very sweet">高度甜味</option>
                                    </select>

                                    <label>苦味</label>
                                    <select class="form-control" name="savor_bitter">
                                        <option value="" selected disabled>請選擇苦味程度
                                            <span class="caret"></span>
                                        </option>
                                        <option value="barely bitter">無苦味</option>
                                        <option value="slightly bitter">微苦味</option>
                                        <option value="medium bitter">中度苦味</option>
                                        <option value="very bitter">高度苦味</option>
                                    </select>

                                    <label>鹹味</label>
                                    <select class="form-control" name="savor_salty">
                                        <option value="" selected disabled>請選擇鹹味程度
                                            <span class="caret"></span>
                                        </option>
                                        <option value="barely salty">無鹹味</option>
                                        <option value="slightly salty">微鹹味</option>
                                        <option value="medium salty">中度鹹味</option>
                                        <option value="very salty">高度鹹味</option>
                                    </select>

                                    <label>鮮味</label>
                                    <select class="form-control" name="savor_fresh">
                                        <option value="" selected disabled>請選擇鮮味程度
                                            <span class="caret"></span>
                                        </option>
                                        <option value="barely fresh">無鮮味</option>
                                        <option value="slightly fresh">微鮮味</option>
                                        <option value="medium fresh">中度鮮味</option>
                                        <option value="very fresh">高度鮮味</option>
                                    </select>

                                    <label>澀味</label>
                                    <select class="form-control" name="savor_astringent">
                                        <option value="" selected disabled>請選擇澀味程度
                                            <span class="caret"></span>
                                        </option>
                                        <option value="barely astringent">無澀味</option>
                                        <option value="slightly astringent">微澀味</option>
                                        <option value="medium astringent">中度澀味</option>
                                        <option value="very astringent">高度澀味</option>
                                    </select>

                                    <label>辣味</label>
                                    <select class="form-control" name="savor_spicy">
                                        <option value="" selected disabled>請選擇辣味程度
                                            <span class="caret"></span>
                                        </option>
                                        <option value="barely spicy">無辣味</option>
                                        <option value="slightly spicy">微辣味</option>
                                        <option value="medium spicy">中度辣味</option>
                                        <option value="very spicy">高度辣味</option>
                                    </select>

                                    <!-- <input type="radio" name="savor_sour" value="barely sour"> 無酸味
                                    <input type="radio" name="savor_sour" value="slightly sour"> 微酸味
                                    <input type="radio" name="savor_sour" value="medium sour"> 中度酸味
                                    <input type="radio" name="savor_sour" value="very sour"> 高度酸味<br/><br/>
                                    <input type="radio" name="savor_sweet" value="barely sweet"> 無甜味
                                    <input type="radio" name="savor_sweet" value="slightly sweet"> 微甜味
                                    <input type="radio" name="savor_sweet" value="medium sweet"> 中度甜味
                                    <input type="radio" name="savor_sweet" value="very sweet"> 高度甜味<br/><br/>
                                    <input type="radio" name="savor_bitter" value="barely bitter"> 無苦味
                                    <input type="radio" name="savor_bitter" value="slightly bitter"> 微苦味
                                    <input type="radio" name="savor_bitter" value="medium bitter"> 中度苦味
                                    <input type="radio" name="savor_bitter" value="very bitter"> 高度苦味<br/><br/>
                                    <input type="radio" name="savor_salty" value="barely salty"> 無鹹味
                                    <input type="radio" name="savor_salty" value="slightly salty"> 微鹹味
                                    <input type="radio" name="savor_salty" value="medium salty"> 中度鹹味
                                    <input type="radio" name="savor_salty" value="very salty"> 高度鹹味<br/><br/>
                                    <input type="radio" name="savor_fresh" value="barely fresh"> 無鮮味
                                    <input type="radio" name="savor_fresh" value="slightly fresh"> 微鮮味
                                    <input type="radio" name="savor_fresh" value="medium fresh"> 中度鮮味
                                    <input type="radio" name="savor_fresh" value="very fresh"> 高度鮮味<br/><br/>
                                    <input type="radio" name="savor_astringent" value="barely astringent"> 無澀味
                                    <input type="radio" name="savor_astringent" value="slightly astringent"> 微澀味
                                    <input type="radio" name="savor_astringent" value="medium astringent"> 中度澀味
                                    <input type="radio" name="savor_astringent" value="very astringent"> 高度澀味<br/><br/>
                                    <input type="radio" name="savor_spicy" value="barely spicy"> 無辛辣味
                                    <input type="radio" name="savor_spicy" value="slightly spicy"> 微辛辣味
                                    <input type="radio" name="savor_spicy" value="medium spicy"> 中度辛辣味
                                    <input type="radio" name="savor_spicy" value="very spicy"> 高度辛辣味<br/><br/><br/><br/> -->
                                <span class="focus-input100"></span>
                            </div>

                            <!-- 口感 -->
                            <div class="validate-input m-b-26">
                                <span class="label-input100">口感：</span>
                                    <select id="ms2" multiple="multiple" name="tastes[]">
                                        <option value="soft">柔軟</option>
                                        <option value="fine and delicate">細緻</option>
                                        <option value="thick and solid">厚實</option>
                                        <option value="light and thin">輕薄</option>
                                        <option value="feeling rock hard">硬感</option>
                                        <option value="coarse feeling">粗感</option>
                                    </select>
                                    <!-- <br>
                                    <input type="checkbox" name="tastes[]" value="soft"> 柔軟
                                    <input type="checkbox" name="tastes[]" value="feeling rock hard"> 硬感
                                    <input type="checkbox" name="tastes[]" value="thick and solid"> 厚實<br/><br/>
                                    <input type="checkbox" name="tastes[]" value="light and thin"> 輕薄
                                    <input type="checkbox" name="tastes[]" value="fine and delicate"> 細緻
                                    <input type="checkbox" name="tastes[]" value="coarse feeling"> 粗感
                                    <br/><br/><br/><br/> -->
                                <span class="focus-input100"></span>
                            </div>

                            <!-- 送出按鈕 -->
                            <br><br><br>
                            <center>
                                <button class="btn primary-btn text-uppercase" type="submit" value="submit" onclick="window.location.href='/form_successful'">完成</button>
                            </center>
                            <br><br><br>
                        </form>
                    </div>
                </div>
            </div>
        </section>
@endsection

@section('css')
    <link rel="stylesheet" href="css/main.css">
    <!-- 表單input的CSS -->
    <link rel="stylesheet" href="css/form/form.css">
    <style type="text/css">
        .checkbox-inline{
            margin-right: 5%;
        }

        .checkbox-left{
            margin-left: 3%;
        }

        .tatse-title{
            margin-right: 5%;
        }

        .radio-inline{
            margin-right: 5%;
        }
        .btn-primary{
            margin-bottom: 5%;
        }
    </style>
    <!-- multiple-select -->
    <link rel="stylesheet" href="css/multiple-select.css">
@endsection

@section('js')
    <!-- pickadate.js v3.5.6 -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/themes/classic.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/themes/classic.date.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/themes/classic.time.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.date.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.time.js"></script>
    <script>
        $( ".example" ).pickadate();
    </script>

    <!-- multiple-select -->
    <script src="js/multiple-select.js"></script>
    <script>
        $(function() {
            $('#ms').change(function() {
                console.log($(this).val());
            }).multipleSelect({
                width: '100%'
            });
        });
    </script>
    <script>
        $(function() {
            $('#ms2').change(function() {
                console.log($(this).val());
            }).multipleSelect({
                width: '100%'
            });
        });
    </script>
@endsection
