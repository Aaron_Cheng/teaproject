<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tea App</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>

    <body>
        <div class="content">
            <h1>Pro Reports</h1>
            <div class="links">
                <a href="{{ url('/add-pro-report-form') }}">Add Pro Report</a>
            </div><br/><br/>
            @foreach ( $proReports as $proReport )
                <button align="center">
                    <a href="/pro-reports/{{ $proReport->id }}" style="text-decoration: none; color: black">
                        <table border="1">
                            <tr>
                                <th>
                                    品名
                                </th>
                                <td>
                                    {{ $proReport->productName }}
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    茶類
                                </th>
                                <td>
                                    {{ $proReport->type }}
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    外型
                                </th>
                                <td>
                                    {{ $proReport->shape }}
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    產區
                                </th>
                                <td>
                                    {{ $proReport->origin }}
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    年分
                                </th>
                                <td>
                                    {{ $proReport->productionYear }}
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    季節
                                </th>
                                <td>
                                    {{ $proReport->season }}
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    品種
                                </th>
                                <td>
                                    {{ $proReport->varieties }}
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    品茶日期
                                </th>
                                <td>
                                    {{ $proReport->reportDate }}
                                </td>
                            </tr>
                        </table>
                    <a/>
                </button><br/><br/><br/>
            @endforeach
        </div>
    </body>
</html>
