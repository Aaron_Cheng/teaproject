@extends('layout.layout')

@section('title','尚未登入')

@section('content')
	<!-- Start home-about Area -->
			<section class="home-about-area pt-120">
				<div class="container">
					<div class="row align-items-center justify-content-between">
						<div class="col-lg-6 col-md-6 home-about-left">
							<img class="img-fluid" src="{{asset('img/logo1.png')}}" alt="">
						</div>
						<div class="col-lg-5 col-md-6 home-about-right">
							<h2>尚未登入~</h2><br>
							<h2>請先前往登入帳號</h2><br>
							<button class="btn btn-primary" onclick="window.location.href='/auth/google'">登入去</button>
						</div>
					</div>
					<br><br>
				</div>
			</section>
@endsection

@section('css')
<link rel="stylesheet" href="css/main.css">
<style type="text/css">
	.input{
		margin-bottom: 20px;
	}
</style>
@endsection

@section('js')
@endsection
