<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>中央大學微課程 | 成果展示</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/achievement/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('css/achievement/business-frontpage.css')}}" rel="stylesheet">

    <style>
    .card-title{
      color:#f05f40;
    }
    .modal-title{
      color:#f05f40;
    }
    .btn{
      outline: none !important;
      box-shadow: none !important;
      -webkit-appearance:none;
    }


    </style>

  </head>

  <body id="page-top">
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v3.0';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

   <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav" style="background-color: #FFF; height: 55px;">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{ url('/') }}"><img src="http://www.ncu.edu.tw/assets/thumbs/pic/df1dfaf0f9e30b8cc39505e1a5a63254.png" height="25" width="25" ><b>自主學習微課程系統</b></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/News') }}"><b>最新公告</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ asset('/introduce') }}"><b>簡介</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/achievement') }}"><b>成果展示</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/record') }}"><b>課程查詢</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/video/video') }}"><b>課程影音</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="{{ url('/application') }}"><b>開課單位登入</b></a>
            </li>
             @if (Auth::guest())
                    <li class="nav-item">
                        <a class="nav-link js-scroll-trigger" href="{{ url('/authrize') }}"><b>管理員登入</b></a>
                    </li>
                    @else
                    <ul class="nav navbar-nav navbar-right ml-auto">
                      <li class="nav-item dropdown">
                        <a href="#" class="dropdown-toggle waves-effect waves-light nav-link js-scroll-trigger" data-toggle="dropdown" role="button" aria-expanded="false">
                          <b>管理員 您好</b>
                          <span class="caret"></span>
                          <small class="tips"></small>
                        </a>
                        <ul class="dropdown-menu">
                          <li>
                            <a href="{{ url('/authrize/menu') }}" class=" waves-effect waves-light nav-link js-scroll-trigger"><i class="fa fa-user" aria-hidden="true"> </i>功能主頁</a>
                          </li>
                          <li class="page-scroll navbtn">
                            <a class=" nav-link js-scroll-trigger " href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-formm').submit();">
                              登出
                            </a>
                            <form id="logout-formm" action="{{ url('/logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                            </form>
                          </li>
                        </ul>
                      </li>
                    </ul>

              @endif

          </ul>
        </div>
      </div>
    </nav>

    <!-- Header with Background Image -->
    <header class="business-header">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
          <br>
          <br>
          <br>
            <h1 class="display-3 text-center text-white mt-4">成果展示</h1>
          </div>
        </div>
      </div>
    </header><br>
    <!-- Page Content -->
      <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">{{$achievements->result_topic}}</h4>
            </div>
            <div class="modal-body">
              <p class="mb_text">成果介紹 :</p>
              <p>{{$achievements->result_intro}}</p>
              <p class="mb_text">發表學期 :</p>
              <p>{{$achievements->term}}</p>
              <p class="mb_text">召集人 :</p>
              <p>{{$achievements->gather_name}}</p>
              <p class="mb_text">系級 :</p>
              <p>{{$achievements->gather_grade}}</p>
              <p class="mb_text">成員 :</p>
              <p>{{$achievements->member1_name}}、{{$achievements->member2_name}}</p>
              <p class="mb_text">社群類別 :</p>
              <p>{{$achievements->field}}</p>
              <p class="mb_text">學習反思與效益 :</p>
              <p>{{$achievements->result_achievement}}</p>
              <p class="mb_text">執行所遇之困難 :</p>
              <p>{{$achievements->result_difficulty}}</p>
              <p class="mb_text">關鍵字 :</p>
              <p>{{$achievements->keyword}}</p>
              <p class="mb_text">作品附件連結 :</p>
              <p><a href="{{$achievements->accociate}}">{{$achievements->accociate}}</a></p>
            </div>
            @if (Auth::guest())

            @else
            <div class="modal-footer">
                <a href="{{ url('/achievement/edit') }}/{{$achievements->id}}">
                  <button type="button" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span> 編輯</button>
                </a>
              <!-- </center> -->
            </div>
            @endif
          </div>
          <div class="fb-comments" data-href="{{$url}}" data-width="1000" data-numposts="5"></div>
          <div class="fb-like" data-href="{{$url}}" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="false"></div>
        </div>
        <div class="col-sm-2"></div>


      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">版權所有 &copy; 大數據暨程式設計研究社 2017</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('vendor/achievement/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/achievement/popper/popper.min.js')}}"></script>
    <script src="{{asset('vendor/achievement/bootstrap/js/bootstrap.min.js')}}"></script>

    <script>

    function openSearch() {

        $("#search").fadeIn("slow");

    }
    function exitSearch() {
      $("#search").fadeOut("slow");
    }
    </script>
  </body>

</html>
