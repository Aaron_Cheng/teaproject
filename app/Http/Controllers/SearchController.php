<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Module_class;
use App\Single_class;
use App\Fractal_class;
use DB;

class SearchController extends Controller
{
    public function index(){
      return view('record.search');
    }
    public function indexall(){
      $single_class=Single_class::all();
      $module_class=Module_class::all();
      $fractal_class=Fractal_class::all();
      return view('record.index');
    }
    public function search(Request $request){
      $name=$request->name;
      $day=$request->weekofday;
      $time=$request->time;
      $tag=$request->tag;
        if($day==0){
          if($tag==null){
            if($name==null){
              $search_single=Single_class::all();
            }else{
              $search_single=Single_class::where('name','like','%'.$name.'%')
                                          ->get();

                                        }
          }else{
            if($name==null){
                $search_single=Single_class::where('keyword','like','%'.$tag.'%')
                                          ->get();

            }else{
                $search_single=Single_class::where('name','like','%'.$name.'%')
                                        ->where('keyword','like','%'.$tag.'%')
                                        ->get();
            }
          }
        }else{
          if($name==null){
            if($tag==null){
              $search_single=Single_class::where('weekday','=',$day)
                                          ->get();
            }else{
              $search_single=Single_class::where('weekday','=',$day)
                                          ->where('keyword','like','%'.$tag.'%')
                                          ->get();
            }

          }else{
            if($tag==null){
              $search_single=Single_class::where('name','like','%'.$name.'%')
                                          ->where('weekday','=',$day)
                                          ->get();
            }else{
              $search_single=Single_class::where('name','like','%'.$name.'%')
                                          ->where('weekday','=',$day)
                                          ->where('keyword','like','%'.$tag.'%')
                                          ->get();
            }
          }
        }
        return back()
          ->with('search_single',$search_single)->with('time',$time);

    }
    public function search2(Request $request){
      $name=$request->name;
      $tag=$request->tag;
      $type=1;
        // if($type==1){
        //     $search_other=Module_class::where('name','like','%'.$name.'%')
        //                                 ->get();
        // }else{
        //     $search_other=Fractal_class::where('name','like','%'.$name.'%')
        //                                 ->get();
        // }
        if($tag==null){
          if($name==null){
            $search_other=Module_class::all();
          }else{
            $search_other=Module_class::where('name','like','%'.$name.'%')
                                        ->get();
          }

        }else{
          if($name==null){
            $search_other=Module_class::where('keyword','like','%'.$tag.'%')
                                        ->get();
          }else{
            $search_other=Module_class::where('name','like','%'.$name.'%')
                                        ->where('keyword','like','%'.$tag.'%')
                                        ->get();
          }

        }

        return back()
          ->with('search_other',$search_other)->with('type',$type);

    }
    public function all(){
      $page=Single_class::paginate(15);
      $single_class=Single_class::paginate(15);
      return view('record.single_class',["single_class"=>$single_class,"page"=>$page]);
    }

    public function all2(){
      $page=Module_class::paginate(15);
      $module_class=Module_class::paginate(15);
      return view('record.module_class',["module_class"=>$module_class,"page"=>$page]);
    }







}
