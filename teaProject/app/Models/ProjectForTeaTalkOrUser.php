<?php

namespace App\Models;

class ProjectForTeaTalkOrUser
{
    public $id;
    public $projectKey;
    public $brandName;
    public $companyName;
    public $productName;
    public $type;
    public $shape;
    public $origin;
    public $productionYear;
    public $season;
    public $varieties;
    public $reportDate;
}
