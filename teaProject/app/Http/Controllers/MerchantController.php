<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Merchant;
use App\Services\MerchantService;

class MerchantController extends Controller
{
    private $merchantService;

    public function __construct(MerchantService $merchantService) {
        $this->merchantService = $merchantService;
    }

    public function getMerchants() {
        $merchants = $this->merchantService->findAll();
        return view('merchant.merchantsInfo', ['merchants' => $merchants]);
    }

    public function getMerchant($merchantId) {
        $merchant = $this->merchantService->findOne($merchantId);
        return view('merchant.merchantInfo', ['merchant' => $merchant]);
    }

    public function addMerchant(Request $request) {
        $merchant = new Merchant();
        $merchant->id = $request->input('id');
        $merchant->brandName = $request->input('brandName');
        $merchant->companyName = $request->input('companyName');
        $merchant->guiNumber = $request->input('guiNumber');
        $merchant->phone = $request->input('phone');
        $merchant->contactPerson = $request->input('contactPerson');
        $merchant->address = $request->input('address');
        $this->merchantService->save($merchant);
    }

    public function updateMerchant(Request $request) {
        $updateMerchantId = $request->input('updateMerchantId');
        $merchant = new Merchant();
        $merchant->id = $request->input('id');
        $merchant->brandName = $request->input('brandName');
        $merchant->companyName = $request->input('companyName');
        $merchant->guiNumber = $request->input('guiNumber');
        $merchant->phone = $request->input('phone');
        $merchant->contactPerson = $request->input('contactPerson');
        $merchant->address = $request->input('address');
        $this->merchantService->update($updateMerchantId, $merchant);
    }

    public function deleteMerchant(Request $request) {
        $deleteMerchantId = $request->input('deleteMerchantId');
        $this->merchantService->delete($deleteMerchantId);
    }

}
