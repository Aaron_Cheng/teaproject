<?php

namespace App\Models;

class ComReportForUser
{
    public $id;
    public $brandName;
    public $productName;
    public $type;
    public $shape;
    public $origin;
    public $productionYear;
    public $season;
    public $varieties;
    public $reportDate;
    public $aromas;
    public $savors;
    public $tastes;
}
