<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\ComReportForProject;
use App\Services\ComReportService;

class ComReportController extends Controller
{
    private $comReportService;

    public function __construct(ComReportService $comReportService) {
        $this->comReportService = $comReportService;
    }

    public function getComReport($comReportId) {
        $comReport = $this->comReportService->findOne($comReportId);
        return view('com-report.comReportInfo', ['comReport' => $comReport]);
    }

    public function getProjectComReports($projectId) {
        $projectComReports = $this->comReportService->findByProjectId($projectId);
        return view('com-report.comReportsInfo', ['projectComReports' => $projectComReports]);
    }

    public function getUserComReports() {
        $userId = session('user')->id;
        $userComReports = $this->comReportService->findByUserId($userId);
        return view('com-report.comReportsInfo', ['comReports' => $userComReports]);
    }

    public function getAddComReportForm(Request $request) {
        $user = $request->session()->get('user');
        if($user !== null) {
            return view('com-report.checkProjectKeyForm');
        } else {
            return view('login.unlogin');
        }
    }

    public function addComReport(Request $request) {
        $userId = $request->session()->get('user')->id;
        $projectKey = $request->session()->get('projectKey');
        if($projectKey !== null) {
            $request->session()->forget('projectKey');
            $comReport = new ComReportForProject();
            $comReport->aromas = $request->input('aromas');
            $savors = [];
            $savors[0] = $request->input('savor_sour');
            $savors[1] = $request->input('savor_sweet');
            $savors[2] = $request->input('savor_bitter');
            $savors[3] = $request->input('savor_salty');
            $savors[4] = $request->input('savor_fresh');
            $savors[5] = $request->input('savor_astringent');
            $savors[6] = $request->input('savor_spicy');
            $comReport->savors = $savors;
            $comReport->tastes = $request->input('tastes');
            $result = $this->comReportService->save($userId, $projectKey, $comReport);
            if($result === 'success') {
                $request->session()->put('userComReports', $this->comReportService->findByUserId($userId));
                return view('form.successful');
            } elseif ($result === 'duplicate') {
                return view('form.AlreadyAdd');
            } elseif ($result === 'fail') {
                print('新增失敗，請檢查所輸入的資料~');
            } else {
                print('新增失敗，請檢查所輸入的資料~');
                print('</br>');
                print('Detail: ' . $result);
            }
        } else {
            print('請重新輸入商用品評單之驗證碼!');
        }
    }

    public function updateComReport(Request $request) {
        $updateComReportId = $request->input('updateComReportId');
        $comReport = new ComReport();
        $comReport->aromas = $request->input('aromas');
        $comReport->savors = $request->input('savors');
        $comReport->tastes = $request->input('tastes');
        $this->comReportService->update($updateComReportId, $comReport);
    }

    public function deleteComReport(Request $request) {
        $deleteComReportId = $request->input('deleteComReportId');
        $this->comReportService->delete($deleteComReportId);
    }

}
