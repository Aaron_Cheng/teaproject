@extends('layouts.app')

@section('title', $single_class->name)

@section('content')
<div class="container col-sm-1"></div>
<div class="container col-sm-10">
  <table>
    <tr>
      <th>課&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
      <th>{{$single_class->name}}</th>
    <tr>
      <td>開課學期</td>
      <td>{{$single_class->term}}</td>
    </tr>
    <tr>
      <td>課程名稱</td>
      <td>{{$single_class->name}}</td>
    </tr>
    <tr>
      <td>課程領域</td>
      <td>{{$single_class->field}}</td>
    </tr>
    <tr>
      <td>其他分類</td>
      <td>{{$single_class->other_field}}</td>
    </tr>
    <tr>
      <td>上課地點</td>
      <td>{{$single_class->location}}</td>
    </tr>
    <tr>
      <td>上課日期</td>
      <td>{{$single_class->date}}</td>
    </tr>
    <tr>
      <td>起始時間</td>
      <td>{{$single_class->start}}</td>
    </tr>
    <tr>
      <td>結束時間</td>
      <td>{{$single_class->end}}</td>
    </tr>
    <tr>
      <td>人數限制</td>
      <td>{{$single_class->limit}}</td>
    </tr>
    <tr>
      <td>講師姓名</td>
      <td>{{$single_class->teacher}}</td>
    </tr>
    <tr>
      <td>聯絡方式</td>
      <td>{{$single_class->email}}</td>
    </tr>
    <tr>
      <td>講師介紹</td>
      <td>{{$single_class->teacher_intro}}</td>
    </tr>
    <tr>
      <td>課程時數</td>
      <td>{{$single_class->class_hr}}</td>
    </tr>
    <tr>
      <td>認證時數</td>
      <td>{{$single_class->auth_hr}}</td>
    </tr>
    <tr>
      <td>課程簡介</td>
      <td>{{$single_class->class_intro}}</td>
    </tr>
    <tr>
      <td>關鍵字</td>
      <td>{{$single_class->keyword}}</td>
    </tr>

  </tr>
</table>
</div>


@endsection

@section('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style>
#other_field{
  display: none;
}
table {
    border-collapse: collapse;
    width: 100%;
    margin-top: 50px;
}

td {
    text-align: left;
    padding: 8px;
}
th{
  font-size: 20px;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #f05f40;
    color: white;
}
</style>
@endsection

@section('js')
<script>
$(document).ready(function(){
  if($("#field").val() === "其他"){
    $("#other_field").show();
  }
});
</script>
@endsection
