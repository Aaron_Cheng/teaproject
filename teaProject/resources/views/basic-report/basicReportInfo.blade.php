<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tea App</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>

    <body>
        <div class="content">
            <table border="1" align="center">
                <tr>
                    <th>
                        Product Name
                    </th>
                    <td>
                        {{ $basicReport->productName }}
                    </td>
                </tr>

                <tr>
                    <th>
                        Type
                    </th>
                    <td>
                        {{ $basicReport->type }}
                    </td>
                </tr>

                <tr>
                    <th>
                        Origin
                    </th>
                    <td>
                        {{ $basicReport->origin }}
                    </td>
                </tr>

                <tr>
                    <th>
                        Production Year
                    </th>
                    <td>
                        {{ $basicReport->productionYear }}
                    </td>
                </tr>

                <tr>
                    <th>
                        Varieties
                    </th>
                    <td>
                        {{ $basicReport->varieties }}
                    </td>
                </tr>

                <tr>
                    <th>
                        Report Date
                    </th>
                    <td>
                        {{ $basicReport->reportDate }}
                    </td>
                </tr>

                <tr>
                    <th>
                        Aromas
                    </th>
                    <td>
                        @foreach ( $basicReport->aromas as $aroma )
                            <li>{{{ $aroma }}}</li>
                        @endforeach
                    </td>
                </tr>

                <tr>
                    <th>
                        Savors
                    </th>
                    <td>
                        @foreach ( $basicReport->savors as $savor )
                            <li>{{{ $savor }}}</li>
                        @endforeach
                    </td>
                </tr>

                <tr>
                    <th>
                        Tastes
                    </th>
                    <td>
                        @foreach ( $basicReport->tastes as $taste )
                            <li>{{{ $taste }}}</li>
                        @endforeach
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
