<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\ProReport;
use App\Services\ProReportService;

class ProReportController extends Controller
{
    private $proReportService;

    public function __construct(ProReportService $proReportService) {
        $this->proReportService = $proReportService;
    }

    public function getProReports() {
        $proReports = $this->proReportService->findAll();
        return view('pro-report.proReportsInfo', ['proReports' => $proReports]);
    }

    public function getProReport($proReportId) {
        $proReport = $this->proReportService->findOne($proReportId);
        return view('pro-report.proReportInfo', ['proReport' => $proReport]);
    }

    public function getUserProReports() {
        $userId = session('user')->id;
        $userProReports = $this->proReportService->findByUserId($userId);
        return view('pro-report.proReportsInfo', ['proReports' => $userProReports]);
    }

    public function getAddProReportForm(Request $request) {
        $user = $request->session()->get('user');
        if($user !== null) {
            return view('pro-report.addProReportForm');
        } else {
            return view('login.unlogin');
        }
    }

    public function addProReport(Request $request) {
        $userId = $request->session()->get('user')->id;
        $proReport = new ProReport();
        $proReport->productName = $request->input('productName');
        $proReport->type = $request->input('type');
        $proReport->shape = $request->input('shape');
        $proReport->origin = $request->input('origin');
        $proReport->productionYear = $request->input('productionYear');
        $proReport->season = $request->input('season');
        $proReport->varieties = $request->input('varieties');
        $proReport->reportDate = $request->input('reportDate');
        $proReport->aromas = $request->input('aromas');
        $savors = [];
        $savors[0] = $request->input('savor_sour');
        $savors[1] = $request->input('savor_sweet');
        $savors[2] = $request->input('savor_bitter');
        $savors[3] = $request->input('savor_salty');
        $savors[4] = $request->input('savor_fresh');
        $savors[5] = $request->input('savor_astringent');
        $savors[6] = $request->input('savor_spicy');
        $proReport->savors = $savors;
        $proReport->tastes = $request->input('tastes');
        $result = $this->proReportService->save($userId, $proReport);
        if($result === 'success') {
            $request->session()->put('userProReports', $this->proReportService->findByUserId($userId));
            return redirect()->action('ShowReportsController@getAllReports');
        } elseif ($result === 'fail') {
            print('新增失敗，請檢查所輸入的資料~');
        } else {
            print('新增失敗，請檢查所輸入的資料~');
            print('</br>');
            print('Detail: ' . $result);
        }
    }

    public function updateProReport(Request $request) {
        $updateProReportId = $request->input('updateProReportId');
        $proReport = new ProReport();
        $proReport->productName = $request->input('productName');
        $proReport->type = $request->input('type');
        $proReport->shape = $request->input('shape');
        $proReport->origin = $request->input('origin');
        $proReport->productionYear = $request->input('productionYear');
        $proReport->season = $request->input('season');
        $proReport->varieties = $request->input('varieties');
        $proReport->reportDate = $request->input('reportDate');
        $proReport->aromas = $request->input('aromas');
        $proReport->savors = $request->input('savors');
        $proReport->tastes = $request->input('tastes');
        $this->proReportService->update($updateProReportId, $proReport);
    }

    public function deleteProReport(Request $request) {
        $deleteProReportId = $request->input('deleteProReportId');
        $this->proReportService->delete($deleteProReportId);
    }

}
