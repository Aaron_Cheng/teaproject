@extends('layout.layout')

@section('title','測試')

@section('content')
    <section class="portfolio-area section-gap" id="portfolio">
        <div class="container">
          	<!-- 標題 -->
    		    <div class="row d-flex justify-content-center">
    		        <div class="menu-content pb-70 col-lg-8">
    		            <div class="title text-center">
    		                <h1 class="mb-10">我的品評單</h1>
    		                <p>記錄下每個品嘗時刻</p>
    		            </div>
    		        </div>
    		    </div>
            <!-- 標題結束 -->

            <!-- 上方選擇標籤 -->
            <div class="filters">
                <ul>
                    <li class="active" data-filter="*">全部</li>
                    <li id='basic-data-filter' data-filter=".basic">初階版</li>
                    <li id='pro-data-filter' data-filter=".pro">專業版</li>
                    <li id='com-data-filter' data-filter=".com">商業版</li>
                    @if ($showReportsFilter === '.basic')
                        <script>
                            $(document).ready(function() {
                                $('#basic-data-filter').click();
                            });
                        </script>
                    @endif

                    @if ($showReportsFilter === '.pro')
                        <script>
                            $(document).ready(function() {
                                $('#pro-data-filter').click();
                            });
                        </script>
                    @endif

                    @if ($showReportsFilter === '.com')
                        <script>
                            $(document).ready(function() {
                                $('#com-data-filter').click();
                            });
                        </script>
                    @endif
                </ul>
            </div>

            <!-- <div class="filters">
                <ul>
                    <li class="active" data-filter="*">全部</li>
                    <li data-filter=".vector">紅茶</li>
                    <li data-filter=".raster">綠茶</li>
                    <li data-filter=".ui">烏龍茶</li>
                    <li data-filter=".printing">黃茶</li>
                    <li data-filter=" ">白茶</li>
                    <li data-filter=" ">黑茶</li>
                </ul>
            </div> -->
            <!-- 上方選擇標籤結束  -->

            <!-- 基礎品評單 -->
            <div class="filters-content">
                <div class="row grid">
                    @foreach ( $basicReports as $basicReport )
                        <form action="/certificate/basic-report-pdf" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="productName" value="{{ $basicReport->productName }}" />
                            <input type="hidden" name="type" value="{{ $basicReport->type }}" />
                            <input type="hidden" name="origin" value="{{ $basicReport->origin }}" />
                            <input type="hidden" name="productionYear" value="{{ $basicReport->productionYear }}" />
                            <input type="hidden" name="varieties" value="{{ $basicReport->varieties }}" />
                            <input type="hidden" name="reportDate" value="{{ $basicReport->reportDate }}" />
                            @foreach ( $basicReport->aromas as $aroma )
                                <input type="hidden" name="aromas[]" value="{{ $aroma }}" />
                            @endforeach

                            @foreach ( $basicReport->savors as $savor )
                                <input type="hidden" name="savors[]" value="{{ $savor }}" />
                            @endforeach

                            @foreach ( $basicReport->tastes as $taste )
                                <input type="hidden" name="tastes[]" value="{{ $taste }}" />
                            @endforeach

                            <div class="single-portfolio col-sm-4 all basic">
                                <div class="relative">
                                    <div class="thumb">
                                        <div class="overlay overlay-bg"></div>
                                        <img class="image img-fluid" src="{{ asset('img/basic.png') }}" alt="">
                                    </div>

                                    <div class="middle">
                                        <div class="text align-self-center d-flex">
                                            <button type='submit'>
                                                <img src="img/preview.png" alt="">
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="p-inner">
                                    <h4>{{ $basicReport->productName }}</h4>
                                    <div class="cat">{{ $basicReport->productionYear }}</div>
                                    <button type="button" class="btn-success"><a href="/certificate-edit"}}" style="color: #fff;">修改</a></button>
                                    <button type="button" class="btn-danger">刪除</button>
                                </div>
                            </div>
                        </form>
                    @endforeach
                </div>
            </div>
            <!-- 基礎品評單結束 -->

            <!-- 專業品評單 -->
            <div class="filters-content">
                <div class="row grid">
                    @foreach ( $proReports as $proReport )
                        <form action="/certificate/pro-report-pdf" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="productName" value="{{ $proReport->productName }}" />
                            <input type="hidden" name="type" value="{{ $proReport->type }}" />
                            <input type="hidden" name="shape" value="{{ $proReport->shape }}" />
                            <input type="hidden" name="origin" value="{{ $proReport->origin }}" />
                            <input type="hidden" name="productionYear" value="{{ $proReport->productionYear }}" />
                            <input type="hidden" name="season" value="{{ $proReport->season }}" />
                            <input type="hidden" name="varieties" value="{{ $proReport->varieties }}" />
                            <input type="hidden" name="reportDate" value="{{ $proReport->reportDate }}" />
                            @foreach ( $proReport->aromas as $aroma )
                                <input type="hidden" name="aromas[]" value="{{ $aroma }}" />
                            @endforeach

                            @foreach ( $proReport->savors as $savor )
                                <input type="hidden" name="savors[]" value="{{ $savor }}" />
                            @endforeach

                            @foreach ( $proReport->tastes as $taste )
                                <input type="hidden" name="tastes[]" value="{{ $taste }}" />
                            @endforeach

                            <div class="single-portfolio col-sm-4 all pro">
                                <div class="relative">
          	                        <div class="thumb">
          	                            <div class="overlay overlay-bg"></div>
          	                            <img class="image img-fluid" src="{{ asset('img/pro.png') }}" alt="">
          	                        </div>

                        						<div class="middle">
                        							  <div class="text align-self-center d-flex">
                                            <button type='submit'>
                                                <img src="img/preview.png" alt="">
                                            </button>
                                        </div>
                        						</div>
                                </div>
                    			<div class="p-inner">
                      				<h4>{{ $proReport->productName }}</h4>
                      				<div class="cat">{{ $proReport->productionYear }}</div>
                                    <button type="button" class="btn-success"><a href="/certificate-edit"}}" style="color: #fff;">修改</a></button>
                                    <button type="button" class="btn-danger">刪除</button>
                    			</div>
                                
                            </div>
                        </form>
                    @endforeach
                </div>
            </div>
            <!-- 專業品評單結束 -->

            <!-- 商用品評單 -->
            <div class="filters-content">
                <div class="row grid">
                    @foreach ( $comReports as $comReport )
                        <form action="/certificate/com-report-pdf" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="productName" value="{{ $comReport->productName }}" />
                            <input type="hidden" name="type" value="{{ $comReport->type }}" />
                            <input type="hidden" name="shape" value="{{ $comReport->shape }}" />
                            <input type="hidden" name="origin" value="{{ $comReport->origin }}" />
                            <input type="hidden" name="productionYear" value="{{ $comReport->productionYear }}" />
                            <input type="hidden" name="season" value="{{ $comReport->season }}" />
                            <input type="hidden" name="varieties" value="{{ $comReport->varieties }}" />
                            <input type="hidden" name="reportDate" value="{{ $comReport->reportDate }}" />
                            @foreach ( $comReport->aromas as $aroma )
                                <input type="hidden" name="aromas[]" value="{{ $aroma }}" />
                            @endforeach

                            @foreach ( $comReport->savors as $savor )
                                <input type="hidden" name="savors[]" value="{{ $savor }}" />
                            @endforeach

                            @foreach ( $comReport->tastes as $taste )
                                <input type="hidden" name="tastes[]" value="{{ $taste }}" />
                            @endforeach

                            <div class="single-portfolio col-sm-4 all com">
                                <div class="relative">
                                    <div class="thumb">
                                        <div class="overlay overlay-bg"></div>
                                        <img class="image img-fluid" src="{{ asset('img/business.png') }}" alt="">
                                    </div>

                                    <div class="middle">
                                        <div class="text align-self-center d-flex">
                                            <button type='submit'>
                                                <img src="img/preview.png" alt="">
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="p-inner">
                                    <h4>{{ $comReport->productName }}</h4>
                                    <div class="cat">{{ $comReport->productionYear }}</div>
                                    <button type="button" class="btn-success"><a href="/certificate-edit"}}" style="color: #fff;">修改</a></button>
                                    <button type="button" class="btn-danger">刪除</button>
                                </div>
                            </div>
                        </form>
                    @endforeach
                </div>
            </div>
            <!-- 商用品評單結束 -->

        </div>
    </section>
@endsection

@section('css')
    <link rel="stylesheet" href="css/main.css">
@endsection

@section('js')
@endsection
