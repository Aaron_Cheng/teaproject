@extends('layout.layout')

@section('title','我的品評單')

@section('content')
    <section class="portfolio-area section-gap" id="portfolio">
        <div class="container">
        	<!-- 標題 -->
		    <div class="row d-flex justify-content-center">
		        <div class="menu-content pb-70 col-lg-8">
		            <div class="title text-center">
		                <h1 class="mb-10">我的品評單</h1>
		                <p>記錄下每個品嘗時刻</p>
		            </div>
		        </div>
		    </div>
            <!-- 標題結束       -->

            <!-- 上方選擇標籤 -->
            <div class="filters">
                <ul>
                    <li class="active" data-filter="*">全部</li>
                    <li data-filter=".vector">紅茶</li>
                    <li data-filter=".raster">綠茶</li>
                    <li data-filter=".ui">烏龍茶</li>
                    <li data-filter=".printing">黃茶</li>
                    <li data-filter=" ">白茶</li>
                    <li data-filter=" ">黑茶</li>
                </ul>
            </div>
            <!-- 上方選擇標籤結束  -->

            <!-- 下方品評單 -->
            <div class="filters-content">
                <div class="row grid">
                	<!-- 第一個 -->
                    <div class="single-portfolio col-sm-4 all vector">
                        <div class="relative">
	                        <div class="thumb">
	                            <div class="overlay overlay-bg"></div>
	                            <img class="image img-fluid" src="{{asset('img/1.jpg')}}" alt="">
	                        </div>
							<a href="{{asset('img/1.jpg')}}" class="img-pop-up">	
								<div class="middle">
									<div class="text align-self-center d-flex"><img src="img/preview.png" alt=""></div>
								</div>
							</a>                              		
                        </div>
						<div class="p-inner">
							<h4>錫蘭紅茶</h4>
							<div class="cat">紅茶</div>
						</div>					                               
                    </div>
                    <!-- 第一個結束 -->

                    <!-- 第二個 -->
                    <div class="single-portfolio col-sm-4 all raster">
                        <div class="relative">
	                        <div class="thumb">
	                            <div class="overlay overlay-bg"></div>
	                            <img class="image img-fluid" src="{{asset('img/1.jpg')}}" alt="">
	                        </div>
							<a href="{{asset('img/1.jpg')}}" class="img-pop-up">	
								<div class="middle">
									<div class="text align-self-center d-flex"><img src="img/preview.png" alt=""></div>
								</div>
							</a>                              		
                        </div>
						<div class="p-inner">
							<h4>西湖龍井</h4>
							<div class="cat">綠茶</div>
						</div>					                               
                    </div>
                    <!-- 第二個結束 --> 

                    <!-- 第三個 -->
                    <div class="single-portfolio col-sm-4 all ui">
                        <div class="relative">
	                        <div class="thumb">
	                            <div class="overlay overlay-bg"></div>
	                            <img class="image img-fluid" src="{{asset('img/1.jpg')}}" alt="">
	                        </div>
							<a href="{{asset('img/1.jpg')}}" class="img-pop-up">	
								<div class="middle">
									<div class="text align-self-center d-flex"><img src="img/preview.png" alt=""></div>
								</div>
							</a> 
	                    </div>
                        <div class="p-inner">
                            <h4>凍頂烏龍茶</h4>
                            <div class="cat">烏龍茶</div>
                        </div>
                    </div>
                    <!-- 第三個結束 -->

                    <!-- 第四個 -->
                    <div class="single-portfolio col-sm-4 all printing">
                        <div class="relative">
	                        <div class="thumb">
	                            <div class="overlay overlay-bg"></div>
	                            <img class="image img-fluid" src="{{asset('img/1.jpg')}}" alt="">
	                        </div>
							<a href="{{asset('img/1.jpg')}}" class="img-pop-up">	
								<div class="middle">
									<div class="text align-self-center d-flex"><img src="img/preview.png" alt=""></div>
								</div>
							</a>                            		
                        </div> 
                        <div class="p-inner">
                            <h4>君山銀針</h4>
                            <div class="cat">黃茶</div>
                        </div>
                    </div>
                    <!-- 第四個結束 -->

                    <!-- 第五個 -->
                    <div class="single-portfolio col-sm-4 all vector">
                        <div class="relative">
                            	<div class="thumb">
	                            	<div class="overlay overlay-bg"></div>
	                            	<img class="image img-fluid" src="{{asset('img/1.jpg')}}" alt="">
	                            </div>
							<a href="{{asset('img/1.jpg')}}" class="img-pop-up">	
								<div class="middle">
									<div class="text align-self-center d-flex"><img src="img/preview.png" alt=""></div>
								</div>
							</a>                             		
                        </div>
                        <div class="p-inner">
                            <h4>大吉嶺紅茶</h4>
                            <div class="cat">紅茶</div>
                        </div>
                    </div>
                    <!-- 第五個結束 -->

                    <!-- 第六個 -->
                    <div class="single-portfolio col-sm-4 all raster">
                        <div class="relative">
	                        <div class="thumb">
	                            <div class="overlay overlay-bg"></div>
	                            <img class="image img-fluid" src="{{asset('img/1.jpg')}}" alt="">
	                        </div>
							<a href="{{asset('img/1.jpg')}}" class="img-pop-up">	
								<div class="middle">
									<div class="text align-self-center d-flex"><img src="img/preview.png" alt=""></div>
								</div>
							</a>                             		
                        </div>
                        <div class="p-inner">
                            <h4>日本抹茶</h4>
                            <div class="cat">綠茶</div>
                        </div>
                    </div>
                    <!-- 第六個結束 -->

                </div>
            </div>
            <!-- 下方品評單結束 -->
        </div>
    </section>	
@endsection

@section('css')
	<link rel="stylesheet" href="css/main.css">				
@endsection

@section('js')
@endsection