<?php

namespace App\Models;

class Merchant
{
    public $id;
    public $brandName;
    public $companyName;
    public $guiNumber;
    public $phone;
    public $contactPerson;
    public $address;
}
