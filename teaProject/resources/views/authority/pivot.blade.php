@extends('layout.layout')

@section('title','資料分析')

@section('content')
	<!-- Start recent-blog Area -->
			<section class="portfolio-area section-gap">
				<div class="container">
					<hr>
					<div style="width: 800px;">
						<div id="output"></div>
					</div>
				</div>	
			</section>
			<!-- end recent-blog Area -->
@endsection

@section('css')
<link rel="stylesheet" href="css/main.css">
<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="css/linearicons.css">
			<link rel="stylesheet" href="css/font-awesome.min.css">
			<link rel="stylesheet" href="css/layout/bootstrap.css">
			<link rel="stylesheet" href="css/layout/magnific-popup.css">
<!-- 			<link rel="stylesheet" href="css/layout/jquery-ui.css">	 -->			
			<link rel="stylesheet" href="css/layout/nice-select.css">							
			<link rel="stylesheet" href="css/layout/animate.min.css">
			<link rel="stylesheet" href="css/layout/owl.carousel.css">

			<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.11/c3.min.css">
			<link rel="stylesheet" type="text/css" href="/dist/pivot.css">

			<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.1.0/styles/color-brewer.min.css"><link href="https://fonts.googleapis.com/css?family=Source+Code+Pro" rel="stylesheet" type="text/css">				

@endsection

@section('js')
	<script src="js/layout/vendor/jquery-2.2.4.min.js"></script>
	<script src="js/layout/popper.min.js"></script>
	<script src="js/layout/vendor/bootstrap.min.js"></script>			
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>			
  	<script src="js/layout/easing.min.js"></script>			
	<script src="js/layout/hoverIntent.js"></script>
	<script src="js/layout/superfish.min.js"></script>	
	<script src="js/layout/jquery.ajaxchimp.min.js"></script>
	<script src="js/layout/jquery.magnific-popup.min.js"></script>	
  	<script src="js/layout/jquery.tabs.min.js"></script>						
	<script src="js/layout/jquery.nice-select.min.js"></script>	
    <script src="js/layout/isotope.pkgd.min.js"></script>			
	<script src="js/layout/waypoints.min.js"></script>
	<script src="js/layout/jquery.counterup.min.js"></script>
	<script src="js/layout/simple-skillbar.js"></script>							
	<script src="js/layout/owl.carousel.min.js"></script>							
	<script src="js/layout/mail-script.js"></script>	
	<script src="js/layout/main.js"></script>


	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/c3/0.4.11/c3.min.js"></script>	

        <script type="text/javascript" src="/dist/pivot.js"></script>
        <script type="text/javascript" src="/dist/export_renderers.js"></script>
        <script type="text/javascript" src="/dist/d3_renderers.js"></script>
        <script type="text/javascript" src="/dist/c3_renderers.js"></script>

        <script type="text/javascript" src="show_code.js"></script>

	<script type="text/javascript">
            // PivotTable
            $(function(){
                var derivers = $.pivotUtilities.derivers;

                var renderers = $.extend(
                    $.pivotUtilities.renderers,
                    $.pivotUtilities.c3_renderers,
                    $.pivotUtilities.d3_renderers,
                    $.pivotUtilities.export_renderers
                    );


                $("#output").pivotUI(
                    [
                        {茶類: "紅茶", 加總: "加總-澀", 性別: "男", 年齡區別: "20歲以下"},
                        {茶類: "紅茶", 加總: "加總-苦", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "紅茶", 加總: "加總-甜", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "紅茶", 加總: "加總-辣", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "紅茶", 加總: "加總-酸", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "紅茶", 加總: "加總-鹹", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "紅茶", 加總: "加總-鹹", 性別: "女", 年齡區別: "30~39"},
                        {茶類: "紅茶", 加總: "加總-鹹", 性別: "女", 年齡區別: "30~39"},
                        {茶類: "紅茶", 加總: "加總-鹹", 性別: "女", 年齡區別: "30~39"},
                        {茶類: "紅茶", 加總: "加總-鹹", 性別: "女", 年齡區別: "30~39"},
                        {茶類: "紅茶", 加總: "加總-鹹", 性別: "女", 年齡區別: "30~39"},

                        {茶類: "白茶", 加總: "加總-鹹", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-苦", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-甜", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-甜", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-甜", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-甜", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-甜", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-甜", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-甜", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-甜", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-甜", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-甜", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-辣", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-酸", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-鹹", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-澀", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-澀", 性別: "女", 年齡區別: "20~29"},
                        {茶類: "白茶", 加總: "加總-澀", 性別: "女", 年齡區別: "20~29"},

                        {茶類: "黑茶", 加總: "加總-苦", 性別: "男", 年齡區別: "30~39"},
                        {茶類: "黑茶", 加總: "加總-甜", 性別: "男", 年齡區別: "30~39"},
                        {茶類: "黑茶", 加總: "加總-甜", 性別: "男", 年齡區別: "30~39"},
                        {茶類: "黑茶", 加總: "加總-甜", 性別: "男", 年齡區別: "30~39"},
                        {茶類: "黑茶", 加總: "加總-甜", 性別: "男", 年齡區別: "30~39"},
                        {茶類: "黑茶", 加總: "加總-澀", 性別: "男", 年齡區別: "30~39"},
                        {茶類: "黑茶", 加總: "加總-澀", 性別: "男", 年齡區別: "30~39"},
                        {茶類: "黃茶", 加總: "加總-甜", 性別: "男", 年齡區別: "40~49"},
                        {茶類: "黃茶", 加總: "加總-甜", 性別: "男", 年齡區別: "40~49"},
                        {茶類: "黃茶", 加總: "加總-甜", 性別: "男", 年齡區別: "40~49"},
                        {茶類: "黃茶", 加總: "加總-甜", 性別: "男", 年齡區別: "40~49"},
                        {茶類: "黃茶", 加總: "加總-酸", 性別: "男", 年齡區別: "40~49"},
                        {茶類: "黃茶", 加總: "加總-酸", 性別: "男", 年齡區別: "40~49"},
                        {茶類: "黃茶", 加總: "加總-酸", 性別: "男", 年齡區別: "40~49"},
                        {茶類: "黃茶", 加總: "加總-酸", 性別: "男", 年齡區別: "40~49"},
                        {茶類: "黃茶", 加總: "加總-酸", 性別: "男", 年齡區別: "40~49"},
                        {茶類: "綠茶", 加總: "加總-酸", 性別: "女", 年齡區別: "50~59"},
                        {茶類: "綠茶", 加總: "加總-酸", 性別: "女", 年齡區別: "50~59"},
                        {茶類: "綠茶", 加總: "加總-酸", 性別: "女", 年齡區別: "50~59"},
                        {茶類: "綠茶", 加總: "加總-酸", 性別: "女", 年齡區別: "50~59"},
                        {茶類: "綠茶", 加總: "加總-甜", 性別: "男", 年齡區別: "20~29"},
                        {茶類: "綠茶", 加總: "加總-澀", 性別: "男", 年齡區別: "20~29"},
                        {茶類: "綠茶", 加總: "加總-澀", 性別: "男", 年齡區別: "20~29"},
                        {茶類: "綠茶", 加總: "加總-鹹", 性別: "男", 年齡區別: "20~29"},
                        {茶類: "綠茶", 加總: "加總-苦", 性別: "男", 年齡區別: "20~29"},
                        {茶類: "綠茶", 加總: "加總-苦", 性別: "男", 年齡區別: "20~29"},
                        {茶類: "綠茶", 加總: "加總-苦", 性別: "男", 年齡區別: "20~29"},
                        {茶類: "綠茶", 加總: "加總-苦", 性別: "男", 年齡區別: "20~29"},
                        {茶類: "綠茶", 加總: "加總-苦", 性別: "男", 年齡區別: "20~29"},
                        {茶類: "綠茶", 加總: "加總-苦", 性別: "男", 年齡區別: "20~29"},
                        {茶類: "烏龍茶", 加總: "加總-辣", 性別: "女", 年齡區別: "60歲以上"},
                        {茶類: "烏龍茶", 加總: "加總-辣", 性別: "女", 年齡區別: "60歲以上"},
                        {茶類: "烏龍茶", 加總: "加總-辣", 性別: "女", 年齡區別: "60歲以上"},
                        {茶類: "烏龍茶", 加總: "加總-辣", 性別: "女", 年齡區別: "60歲以上"},
                        {茶類: "烏龍茶", 加總: "加總-辣", 性別: "女", 年齡區別: "60歲以上"},

                    ],
                    {
                        rows: ["茶類"],
                        cols: ["加總"],
                        sex: ["性別"],
                        age: ["年齡區別"],
                    }
                );
             });
        </script>
@endsection