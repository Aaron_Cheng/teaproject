<?php

namespace App\Models;

class User
{
    public $id;
    public $userName;
    public $nationality;
    public $sex;
    public $birth;
    public $education;
    public $email;
    public $phone;
    public $residence;
    public $profession;
    public $frequency;
    public $hobbies;
    public $preferenceFragrances;
    public $preferenceTeaTypes;
}
