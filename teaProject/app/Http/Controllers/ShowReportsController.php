<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\BasicReport;
use App\Models\ProReport;
use App\Services\BasicReportService;
use App\Services\ProReportService;
use App\Services\ComReportService;

class ShowReportsController extends Controller
{
    private $basicReportService;

    public function __construct(BasicReportService $basicReportService, ProReportService $proReportService, ComReportService $comReportService) {
        $this->basicReportService = $basicReportService;
        $this->proReportService = $proReportService;
        $this->comReportService = $comReportService;
    }

    public function getData() {
        $userId = session('user')->id;
        session(['userBasicReports' => $this->basicReportService->findByUserId($userId)]);
        session(['userProReports' => $this->proReportService->findByUserId($userId)]);
        session(['userComReports' => $this->comReportService->findByUserId($userId)]);
        session(['userReportsInSession' => 'get']);
    }

    public function getAllReports(Request $request) {
        $user = $request->session()->get('user');
        if($user !== null) {
            if($request->session()->get('userReportsInSession') === null) {
                $this->getData();
            }
            $userBasicReports = $request->session()->get('userBasicReports');
            $userProReports = $request->session()->get('userProReports');
            $userComReports = $request->session()->get('userComReports');
            return view('certificate.test', ['basicReports' => $userBasicReports, 'proReports' => $userProReports, 'comReports' => $userComReports, 'showReportsFilter' => '*']);
        } else {
            return view('login.unlogin');
        }
    }

    public function getBasicReportPdf(Request $request) {
        $userName = session('user')->userName;
        $productName = $request->input('productName');
        $type = $request->input('type');
        $origin = $request->input('origin');
        $productionYear = $request->input('productionYear');
        $varieties = $request->input('varieties');
        $reportDate = $request->input('reportDate');
        $aromas = $request->input('aromas');
        $savors = $request->input('savors');
        $tastes = $request->input('tastes');

        $pdf = new \setasign\Fpdi\Fpdi();
        $pdf->setSourceFile('img/certificate_basic.pdf');
        $tpl = $pdf->importPage(1);
        $pdf->AddPage();
        $pdf->useTemplate($tpl);
        $pdf->SetFont('Arial');
        $pdf->SetTextColor(8, 60, 90);

        // Product Name
        $pdf->SetFontSize('12');
        $pdf->SetXY(103.5, 67.25);
        $pdf->Cell(0, 0, $productName, 0, 0, 'L');

        // Type
        $pdf->SetFontSize('12');
        $pdf->SetXY(80, 73.6);
        $pdf->Cell(0, 0, $type, 0, 0, 'L');

        // Origin
        $pdf->SetFontSize('12');
        $pdf->SetXY(127.75, 74.4);
        $pdf->Cell(0, 0, $origin, 0, 0, 'L');

        // Production Year
        $pdf->SetFontSize('12');
        $pdf->SetXY(80, 81.6);
        $pdf->Cell(0, 0, $productionYear, 0, 0, 'L');

        // Varieties
        $pdf->SetFontSize('12');
        $pdf->SetXY(127.75, 81.75);
        $pdf->Cell(0, 0, $varieties, 0, 0, 'L');

        // User Name
        $pdf->SetFontSize('12');
        $pdf->SetXY(71, 243.5);
        $pdf->Cell(0, 0, $userName, 0, 0, 'L');

        // Report Date
        $pdf->SetFontSize('12');
        $pdf->SetXY(129, 242.7);
        $pdf->Cell(0, 0, $reportDate, 0, 0, 'L');

        // Aromas
        $startY = 142;
        foreach ($aromas as $aroma) {
            $pdf->SetFontSize('12');
            $pdf->SetXY(42, $startY += 8);
            $pdf->Cell(0, 0, $aroma, 0, 0, 'L');
        }

        // Savors
        $startY = 142;
        foreach ($savors as $savor) {
            $pdf->SetFontSize('12');
            $pdf->SetXY(91, $startY += 8);
            $pdf->Cell(0, 0, $savor, 0, 0, 'L');
        }

        // Tastes
        $startY = 142;
        foreach ($tastes as $taste) {
            $pdf->SetFontSize('12');
            $pdf->SetXY(143, $startY += 8);
            $pdf->Cell(0, 0, $taste, 0, 0, 'L');
        }

        $pdf->Output();
    }

    public function getProReportPdf(Request $request) {
        $userName = session('user')->userName;
        $productName = $request->input('productName');
        $type = $request->input('type');
        $shape = $request->input('shape');
        $origin = $request->input('origin');
        $productionYear = $request->input('productionYear');
        $season = $request->input('season');
        $varieties = $request->input('varieties');
        $reportDate = $request->input('reportDate');
        $aromas = $request->input('aromas');
        $savors = $request->input('savors');
        $tastes = $request->input('tastes');

        $pdf = new \setasign\Fpdi\Fpdi();
        $pdf->setSourceFile('img/certificate_pro.pdf');
        $tpl = $pdf->importPage(1);
        $pdf->AddPage();
        $pdf->useTemplate($tpl);
        $pdf->SetFont('Arial');
        $pdf->SetTextColor(8, 60, 90);

        // Product Name
        $pdf->SetFontSize('12');
        $pdf->SetXY(103.5, 67.25);
        $pdf->Cell(0, 0, $productName, 0, 0, 'L');

        // Type
        $pdf->SetFontSize('12');
        $pdf->SetXY(67.25, 73.6);
        $pdf->Cell(0, 0, $type, 0, 0, 'L');

        // Shape
        $pdf->SetFontSize('12');
        $pdf->SetXY(103.5, 74.5);
        $pdf->Cell(0, 0, $shape, 0, 0, 'L');

        // Origin
        $pdf->SetFontSize('12');
        $pdf->SetXY(137.2, 74.4);
        $pdf->Cell(0, 0, $origin, 0, 0, 'L');

        // Production Year
        $pdf->SetFontSize('12');
        $pdf->SetXY(67.25, 81.6);
        $pdf->Cell(0, 0, $productionYear, 0, 0, 'L');

        // Season
        $pdf->SetFontSize('12');
        $pdf->SetXY(103.5, 82.25);
        $pdf->Cell(0, 0, $season, 0, 0, 'L');

        // Varieties
        $pdf->SetFontSize('12');
        $pdf->SetXY(137.2, 81.75);
        $pdf->Cell(0, 0, $varieties, 0, 0, 'L');

        // User Name
        $pdf->SetFontSize('12');
        $pdf->SetXY(71, 243.5);
        $pdf->Cell(0, 0, $userName, 0, 0, 'L');

        // Report Date
        $pdf->SetFontSize('12');
        $pdf->SetXY(129, 242.7);
        $pdf->Cell(0, 0, $reportDate, 0, 0, 'L');

        // Aromas
        $startY = 142;
        foreach ($aromas as $aroma) {
            $pdf->SetFontSize('12');
            $pdf->SetXY(42, $startY += 8);
            $pdf->Cell(0, 0, $aroma, 0, 0, 'L');
        }

        // Savors
        $startY = 142;
        foreach ($savors as $savor) {
            $pdf->SetFontSize('12');
            $pdf->SetXY(91, $startY += 8);
            $pdf->Cell(0, 0, $savor, 0, 0, 'L');
        }

        // Tastes
        $startY = 142;
        foreach ($tastes as $taste) {
            $pdf->SetFontSize('12');
            $pdf->SetXY(143, $startY += 8);
            $pdf->Cell(0, 0, $taste, 0, 0, 'L');
        }

        $pdf->Output();
    }

    public function getComReportPdf(Request $request) {
        $userName = session('user')->userName;
        $productName = $request->input('productName');
        $type = $request->input('type');
        $shape = $request->input('shape');
        $origin = $request->input('origin');
        $productionYear = $request->input('productionYear');
        $season = $request->input('season');
        $varieties = $request->input('varieties');
        $reportDate = $request->input('reportDate');
        $aromas = $request->input('aromas');
        $savors = $request->input('savors');
        $tastes = $request->input('tastes');

        $pdf = new \setasign\Fpdi\Fpdi();
        $pdf->setSourceFile('img/certificate_com.pdf');
        $tpl = $pdf->importPage(1);
        $pdf->AddPage();
        $pdf->useTemplate($tpl);
        $pdf->SetFont('Arial');
        $pdf->SetTextColor(8, 60, 90);

        // Product Name
        $pdf->SetFontSize('12');
        $pdf->SetXY(103.5, 67.25);
        $pdf->Cell(0, 0, $productName, 0, 0, 'L');

        // Type
        $pdf->SetFontSize('12');
        $pdf->SetXY(67.25, 73.6);
        $pdf->Cell(0, 0, $type, 0, 0, 'L');

        // Shape
        $pdf->SetFontSize('12');
        $pdf->SetXY(103.5, 74.5);
        $pdf->Cell(0, 0, $shape, 0, 0, 'L');

        // Origin
        $pdf->SetFontSize('12');
        $pdf->SetXY(137.2, 74.4);
        $pdf->Cell(0, 0, $origin, 0, 0, 'L');

        // Production Year
        $pdf->SetFontSize('12');
        $pdf->SetXY(67.25, 81.6);
        $pdf->Cell(0, 0, $productionYear, 0, 0, 'L');

        // Season
        $pdf->SetFontSize('12');
        $pdf->SetXY(103.5, 82.25);
        $pdf->Cell(0, 0, $season, 0, 0, 'L');

        // Varieties
        $pdf->SetFontSize('12');
        $pdf->SetXY(137.2, 81.75);
        $pdf->Cell(0, 0, $varieties, 0, 0, 'L');

        // User Name
        $pdf->SetFontSize('12');
        $pdf->SetXY(71, 243.5);
        $pdf->Cell(0, 0, $userName, 0, 0, 'L');

        // Report Date
        $pdf->SetFontSize('12');
        $pdf->SetXY(129, 242.7);
        $pdf->Cell(0, 0, $reportDate, 0, 0, 'L');

        // Aromas
        $startY = 142;
        foreach ($aromas as $aroma) {
            $pdf->SetFontSize('12');
            $pdf->SetXY(42, $startY += 8);
            $pdf->Cell(0, 0, $aroma, 0, 0, 'L');
        }

        // Savors
        $startY = 142;
        foreach ($savors as $savor) {
            $pdf->SetFontSize('12');
            $pdf->SetXY(91, $startY += 8);
            $pdf->Cell(0, 0, $savor, 0, 0, 'L');
        }

        // Tastes
        $startY = 142;
        foreach ($tastes as $taste) {
            $pdf->SetFontSize('12');
            $pdf->SetXY(143, $startY += 8);
            $pdf->Cell(0, 0, $taste, 0, 0, 'L');
        }

        $pdf->Output();
    }
}
