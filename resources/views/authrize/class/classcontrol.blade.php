@extends('layouts.app')

@section('title', '管理已開設課程')

@section('content')
<div class="container">
  <h2>
    <b></b>
  </h2>
</div>

<div class="container col-sm-1">
</div>

<div class="container col-sm-10">
  <br>
  <!-- 登入歡迎訊息 -->
  <div class="alert alert-info alert-dismissable  fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <span class="glyphicon glyphicon-user"></span>
    <strong>在這管理所有已開立的課程資訊</strong>
  </div>

  <div class="well form-horizontal">
    <fieldset>

      <!-- title -->
      <legend>
        <b>管理所有已開設課程</b>
      </legend>

      <!-- course table -->
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>課程名稱</th>
              <th>課程類別</th>
              <th>講師名稱</th>
              <th>聯絡方式</th>
              <th>申請時間</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($single_classes as $single_class)
              <tr class="success">
                <td>{{$single_class->name}}</td>
                <td>個別課程</td>
                <td>{{$single_class->teacher}}</td>
                <td>{{$single_class->email}}</td>
                <?php
                  $timestamp = strtotime($single_class->created_at) + 8*60*60;
                  $time = date('Y-m-d H:i', $timestamp);
                ?>
                <td>{{$time}}</td>
                <!-- view -->
                <td>
                  <a href="{{ url('/application/view_single') }}/{{$single_class->id}}">
                    <button type="button" class="btn btn-default btn-sm">
                      <span class="glyphicon glyphicon-eye-open"></span> 查看詳情
                    </button>
                  </a>
                </td>
                <!-- edit -->
                <td>
                  <a href="{{ url('/application/edit_single') }}/{{$single_class->id}}">
                    <button type="button" class="btn btn-warning btn-sm">
                      <span class="glyphicon glyphicon-pencil"></span> 編輯
                    </button>
                  </a>
                </td>
                <!-- del -->
                <td>
                  <a href="#" data-toggle="modal" data-target="#deletesingle{{$single_class->id}}">
                    <button type="button" class="btn btn-danger btn-sm">
                      <span class="glyphicon glyphicon-trash"></span> 刪除
                    </button>
                  </a>
                </td>
              </tr>
              <!-- delete single -->
              <div class="modal fade" id="deletesingle{{$single_class->id}}" role="dialog">

                  <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">確認刪除?</h4>
                      </div>
                      <div class="modal-body">
                          <p>刪除的動作將無法復原</p>
                      </div>
                      <div class="modal-footer">
                          <center>
                            <a href="{{asset('/application/deleteSingle_auth')}}/{{$single_class->id}}">
                              <button type="submit" class="btn btn-danger">
                                <span class="glyphicon glyphicon-trash"></span> 確認刪除
                              </button>
                            </a>
                          </center>
                      </div>
                    </div>
                  </div>
                </div>
            @endforeach
            @foreach ($module_classes as $module_class)
            <tr class="info">
              <td>{{$module_class->name}}</td>
              <td>模組課程</td>
              <td>{{$module_class->teacher}}</td>
              <td>{{$module_class->email}}</td>
              <?php
                $timestamp = strtotime($module_class->created_at) + 8*60*60;
                $time = date('Y-m-d H:i', $timestamp);
              ?>
              <td>{{$time}}</td>
              <!-- view -->
              <td>
                <a href="{{ url('/application/view_module') }}/{{$module_class->id}}">
                  <button type="button" class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-eye-open"></span> 查看詳情
                  </button>
                </a>
              </td>
              <!-- edit -->
              <td>
                <a href="{{ url('/application/edit_module') }}/{{$module_class->id}}">
                  <button type="button" class="btn btn-warning btn-sm">
                    <span class="glyphicon glyphicon-pencil"></span> 編輯
                  </button>
                </a>
              </td>
              <!-- del -->
              <td>
                <a href="#" data-toggle="modal" data-target="#deletemodule{{$module_class->id}}">
                  <button type="button" class="btn btn-danger btn-sm">
                    <span class="glyphicon glyphicon-trash"></span> 刪除
                  </button>
                </a>
              </td>
            </tr>
            <!-- delete module -->
            <div class="modal fade" id="deletemodule{{$module_class->id}}" role="dialog">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">確認刪除?</h4>
                    </div>
                    <div class="modal-body">
                        <p>刪除的動作將無法復原</p>
                    </div>
                    <div class="modal-footer">
                        <center>
                          <a href="{{asset('/application/deleteModule_auth')}}/{{$module_class->id}}">
                            <button type="submit" class="btn btn-danger">
                              <span class="glyphicon glyphicon-trash"></span> 確認刪除
                            </button>
                          </a>
                        </center>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
            @foreach ($fractal_classes as $fractal_class)
            <tr class="warning">
              <td>{{$fractal_class->name}}</td>
              <td>碎形課程</td>
              <td>{{$fractal_class->teacher}}</td>
              <td>{{$fractal_class->email}}</td>
              <?php
                $timestamp = strtotime($fractal_class->created_at) + 8*60*60;
                $time = date('Y-m-d H:i', $timestamp);
              ?>
              <td>{{$time}}</td>
              <!-- view -->
              <td>
                <a href="{{ url('/application/view_fractal') }}/{{$fractal_class->id}}">
                  <button type="button" class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-eye-open"></span> 查看詳情
                  </button>
                </a>
              </td>
              <!-- edit -->
              <td>
                <a href="{{ url('/application/edit_fractal') }}/{{$fractal_class->id}}">
                  <button type="button" class="btn btn-warning btn-sm">
                    <span class="glyphicon glyphicon-pencil"></span> 編輯
                  </button>
                </a>
              </td>
              <!-- del -->
              <td>
                <a href="#" data-toggle="modal" data-target="#deletefractal{{$fractal_class->id}}">
                  <button type="button" class="btn btn-danger btn-sm">
                    <span class="glyphicon glyphicon-trash"></span> 刪除
                  </button>
                </a>
              </td>
            </tr>
            <!-- delete fractal -->
            <div class="modal fade" id="deletefractal{{$fractal_class->id}}" role="dialog">
                <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">確認刪除?</h4>
                    </div>
                    <div class="modal-body">
                        <p>刪除的動作將無法復原</p>
                    </div>
                    <div class="modal-footer">
                        <center>
                          <a href="{{asset('/application/deleteFractal')}}/{{$fractal_class->id}}">
                            <button type="submit" class="btn btn-danger">
                              <span class="glyphicon glyphicon-trash"></span> 確認刪除
                            </button>
                          </a>
                        </center>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          </tbody>
        </table>
      </div>

      <!-- Button 新增課程-->
      <!-- <div class="form-group">
        <center>
          <a href="{{ url('/application/choose') }}">
            <button type="button" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> 新增課程</button>
          </a>
          <a href="{{ url('/application/logout') }}">
            <button type="button" class="btn btn-primary btn-sm" id="logout"><span class="glyphicon glyphicon-off"></span> 登出</button>
          </a>
          <a href="{{ url('/application/edit_pwd') }}">
            <button type="button" class="btn btn-basic btn-sm" id="edit_pwd"><span class="glyphicon glyphicon-cog"></span> 編輯密碼 </button>
          </a>
        </center>
      </div> -->

    </fieldset>
  </div>
</div>
@endsection

@section('css')
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<style>
#new_class {
  /*float: right;*/
  /*background-color:#33cccc;*/
  /*border-color: #33cccc;*/
  /*transition-duration: 0.4s;*/
}
#new_class:hover {
  /*float: right;*/
  /*color:#ffffff;*/
  /*background-color:#248f8f;*/
  /*border-color: #248f8f;*/
}
#logout {
  /*float: right;*/
  background-color:#33cccc;
  border-color: #33cccc;
  transition-duration: 0.4s;
}
#logout:hover {
  /*float: right;*/
  color:#ffffff;
  background-color:#248f8f;
  border-color: #248f8f;
}
#edit_pwd{
  /*float: right;*/
  background-color: #808080;
  border-color: #808080;
  color: #ffffff;
}
#edit_pwd:hover{
  /*float: right;*/
  background-color:#737373;
  border-color: #595959;
  color: #ffffff;
}
#act{
  float:right;
}
</style>
@endsection

@section('js')
<script>

</script>
@endsection
