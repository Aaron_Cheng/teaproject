@extends('layout.layout-authority')

@section('title','後台管理')

@section('content')
	<!-- Start recent-blog Area -->
			<section class="portfolio-area section-gap">
				<div class="container">
					<div class="row justify-content-center">
						<div class="pb-30 header-text">
							<h2><center>管理者頁面</center></h2>
						</div>
					</div>
					<hr>
					<div class="row">	
						<div class="single-recent-blog col-lg-4 col-md-4">
							<div class="thumb">
								<img class="f-img img-fluid mx-auto" src="{{asset('img/tea2.jpg')}}" alt="">	
							</div>
							<div class="bottom d-flex justify-content-between align-items-center flex-wrap">
							</div>							
							<a href="#">
								<h4><center>會員管理</center></h4>
							</a>
						</div>
						<div class="single-recent-blog col-lg-4 col-md-4">
							<div class="thumb">
								<img class="f-img img-fluid mx-auto" src="{{asset('img/tea3.jpg')}}" alt="">	
							</div>
							<div class="bottom d-flex justify-content-between align-items-center flex-wrap">
							</div>							
							<a href="#">
								<h4><center>建立商業版品評單</center></h4>
							</a>
						</div>
						<div class="single-recent-blog col-lg-4 col-md-4">
							<div class="thumb">
								<img class="f-img img-fluid mx-auto" src="{{asset('img/tea4.jpg')}}" alt="">	
							</div>
							<div class="bottom d-flex justify-content-between align-items-center flex-wrap">
							</div>							
							<a href="#">
								<h4><center>資料分析</center></h4>
							</a>
						</div>												
											
												
					</div>
				</div>	
			</section>
			<!-- end recent-blog Area -->
@endsection

@section('css')
<link rel="stylesheet" href="css/main.css">
<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="css/linearicons.css">
			<link rel="stylesheet" href="css/font-awesome.min.css">
			<link rel="stylesheet" href="css/layout/bootstrap.css">
			<link rel="stylesheet" href="css/layout/magnific-popup.css">
<!-- 			<link rel="stylesheet" href="css/layout/jquery-ui.css">	 -->			
			<link rel="stylesheet" href="css/layout/nice-select.css">							
			<link rel="stylesheet" href="css/layout/animate.min.css">
			<link rel="stylesheet" href="css/layout/owl.carousel.css">				

@endsection

@section('js')
	<script src="js/layout/vendor/jquery-2.2.4.min.js"></script>
	<script src="js/layout/popper.min.js"></script>
	<script src="js/layout/vendor/bootstrap.min.js"></script>			
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>			
  	<script src="js/layout/easing.min.js"></script>			
	<script src="js/layout/hoverIntent.js"></script>
	<script src="js/layout/superfish.min.js"></script>	
	<script src="js/layout/jquery.ajaxchimp.min.js"></script>
	<script src="js/layout/jquery.magnific-popup.min.js"></script>	
  	<script src="js/layout/jquery.tabs.min.js"></script>						
	<script src="js/layout/jquery.nice-select.min.js"></script>	
    <script src="js/layout/isotope.pkgd.min.js"></script>			
	<script src="js/layout/waypoints.min.js"></script>
	<script src="js/layout/jquery.counterup.min.js"></script>
	<script src="js/layout/simple-skillbar.js"></script>							
	<script src="js/layout/owl.carousel.min.js"></script>							
	<script src="js/layout/mail-script.js"></script>	
	<script src="js/layout/main.js"></script>	
@endsection