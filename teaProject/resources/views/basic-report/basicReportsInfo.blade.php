<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tea App</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>

    <body>
        <div class="content">
            <h1>Basic Reports</h1>
            <div class="links">
                <a href="{{ url('/add-basic-report-form') }}">Add Basic Report</a>
            </div><br/><br/>
            @foreach ( $basicReports as $basicReport )
                <button align="center">
                    <a href="/basic-reports/{{ $basicReport->id }}" style="text-decoration: none; color: black">
                        <table border="1">
                            <tr>
                                <th>
                                    Product Name
                                </th>
                                <td>
                                    {{ $basicReport->productName }}
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    Type
                                </th>
                                <td>
                                    {{ $basicReport->type }}
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    Origin
                                </th>
                                <td>
                                    {{ $basicReport->origin }}
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    Production Year
                                </th>
                                <td>
                                    {{ $basicReport->productionYear }}
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    Varieties
                                </th>
                                <td>
                                    {{ $basicReport->varieties }}
                                </td>
                            </tr>

                            <tr>
                                <th>
                                    Report Date
                                </th>
                                <td>
                                    {{ $basicReport->reportDate }}
                                </td>
                            </tr>
                        </table>
                    </a>
                </button><br/><br/><br/>
            @endforeach
        </div>
    </body>
</html>
