@extends('layout.layout')

@section('title','個人檔案')

@section('content')
		<section class="home-about-area pt-120">
			<div class="container">
				<div class="row align-items-center justify-content-between">
					<div class="col-lg-6 col-md-6 home-about-left">
						<img class="img-fluid" src="{{asset('img/logo1.png')}}" alt="">
					</div>
					<div class="col-lg-5 col-md-6 home-about-right">
						<p>姓名 : 王小明</p>
						<p>國籍 : 台灣</p>
						<p>性別 : 男</p>
						<p>出生日期 : 2000/02/02</p>
						<p>教育程度 : 大專院校</p>
						<p>信箱 : 123456789@gmail.com</p>
						<p>電話 : 0912345678</p>
						<p>地址 : 桃園市中壢區中大路300號</p>
						<p>職業: 學生</p>
						<p>喝茶的頻率 : 每天</p>
						<p>興趣 : 打籃球</p>
						<button class="btn btn-primary" onclick="window.location.href='/profile_modify'">前往修改</button>
					</div>
				</div>
				<br><br>
			</div>	
		</section>
@endsection

@section('css')
<link rel="stylesheet" href="css/main.css">
@endsection

@section('js')
@endsection