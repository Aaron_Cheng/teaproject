<?php

namespace App\Models;

class ProReport
{
    public $id;
    public $productName;
    public $type;
    public $shape;
    public $origin;
    public $productionYear;
    public $season;
    public $varieties;
    public $reportDate;
    public $aromas;
    public $savors;
    public $tastes;
}
