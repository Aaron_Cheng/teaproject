<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

use App\Models\ProjectForTeaTalkOrUser;
use App\Models\ProjectForMerchant;

class ProjectService
{
    private $url = "http://140.115.81.245:8080/";
    // private $url = "http://127.0.0.1:8080/";
    private $client;

    public function __construct(Client $client) {
        $this->client = $client;
    }

    public function findAll() {
        $response = $this->client->get($this->url.'projects');
        $projectsJsonStr = $response->getBody()->getContents();
        $projectsJsonObj = json_decode($projectsJsonStr);
        $projects = collect();
        foreach ($projectsJsonObj as $projectJsonObj) {
            $projects->push($this->jsonObjToProjectForTeaTalkOrUser($projectJsonObj));
        }
        return $projects;
    }

    public function findOneForTeaTalk($projectId) {
        $response = $this->client->get($this->url.'projects/'.$projectId);
        $projectJsonStr = $response->getBody()->getContents();
        $projectJsonObj = json_decode($projectJsonStr);
        $proejct = $this->jsonObjToProjectForTeaTalkOrUser($projectJsonObj);
        return $proejct;
    }

    public function findOneByProjectKey($inputKey) {
        $response = $this->client->post($this->url.'projects/projectKey', ['body' => $inputKey]);
        $projectJsonStr = $response->getBody()->getContents();
        if($projectJsonStr !== 'null') {
            $projectJsonObj = json_decode($projectJsonStr);
            $project = $this->jsonObjToProjectForTeaTalkOrUser($projectJsonObj);
            return $project;
        } else {
            return null;
        }
    }

    public function findByMerchantId($merchantId) {
        $response = $this->client->get($this->url.'merchants/'.$merchantId.'/projects');
        $projectsJsonStr = $response->getBody()->getContents();
        $projectsJsonObj = json_decode($projectsJsonStr);
        $projects = collect();
        foreach ($projectsJsonObj as $projectJsonObj) {
            $projects->push($this->jsonObjToProjectForMerchant($projectJsonObj));
        }
        return $projects;
    }

    public function findOneForMerchant($merchantId, $projectId) {
        $response = $this->client->get($this->url.'merchants/'.$merchantId.'/projects/'.$projectId);
        $projectJsonStr = $response->getBody()->getContents();
        $projectJsonObj = json_decode($projectJsonStr);
        $proejct = $this->jsonObjToProjectForMerchant($projectJsonObj);
        return $proejct;
    }

    public function save($merchantId, $project) {
        $projectJsonStr = json_encode($project);
        $response = $this->client->post($this->url.'merchants/'.$merchantId.'/projects', ['body' => $projectJsonStr]);
    }

    public function update($updateProjectId, $project) {
        $projectJsonStr = json_encode($project);
        $response = $this->client->put($this->url.'projects/'.$updateProjectId, ['body' => $projectJsonStr]);
    }

    public function delete($deleteProjectId) {
        $response = $this->client->delete($this->url.'projects/'.$deleteProjectId);
    }

    private function jsonObjToProjectForTeaTalkOrUser($projectJsonObj) {
        $projectForTeaTalkOrUser = new ProjectForTeaTalkOrUser();
        $projectForTeaTalkOrUser->id = $projectJsonObj->id;
        $projectForTeaTalkOrUser->projectKey = $projectJsonObj->projectKey;
        $projectForTeaTalkOrUser->brandName = $projectJsonObj->brandName;
        $projectForTeaTalkOrUser->companyName = $projectJsonObj->companyName;
        $projectForTeaTalkOrUser->productName = $projectJsonObj->productName;
        $projectForTeaTalkOrUser->type = $projectJsonObj->type;
        $projectForTeaTalkOrUser->shape = $projectJsonObj->shape;
        $projectForTeaTalkOrUser->origin = $projectJsonObj->origin;
        $projectForTeaTalkOrUser->productionYear = $projectJsonObj->productionYear;
        $projectForTeaTalkOrUser->season = $projectJsonObj->season;
        $projectForTeaTalkOrUser->varieties = $projectJsonObj->varieties;
        $projectForTeaTalkOrUser->reportDate = $projectJsonObj->reportDate;
        return $projectForTeaTalkOrUser;
    }

    private function jsonObjToProjectForMerchant($projectJsonObj) {
        $projectForMerchant = new ProjectForMerchant();
        $projectForMerchant->id = $projectJsonObj->id;
        $projectForMerchant->productName = $projectJsonObj->productName;
        $projectForMerchant->type = $projectJsonObj->type;
        $projectForMerchant->shape = $projectJsonObj->shape;
        $projectForMerchant->origin = $projectJsonObj->origin;
        $projectForMerchant->productionYear = $projectJsonObj->productionYear;
        $projectForMerchant->season = $projectJsonObj->season;
        $projectForMerchant->varieties = $projectJsonObj->varieties;
        $projectForMerchant->reportDate = $projectJsonObj->reportDate;
        return $projectForMerchant;
    }

}
