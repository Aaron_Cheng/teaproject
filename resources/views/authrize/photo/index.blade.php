@extends('layouts.app')

@section('title', '最新公告管理')

@section('content')
<br><h2 style="border-left:solid 2px #e6b3b3">&nbsp;最新公告圖片管理</h2><br>
      <div class="right"><a href="{{ url('/authrize/menu/photo/create') }}" ><button type="button" class="btn btn-primary">新增</button></a></div>
<hr>
@if ($success = Session::get('success'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
      <strong>成功刪除{{Session::get('title')}}</strong>
  </div>
@endif
@foreach($photo as $photos)
<div class="row content">
    <div class="col-sm-1 ">
          <form action="{{ asset('authrize/menu/photo/'.$photos->id) }}" method="POST">
                 {!! csrf_field() !!}
                 {!! method_field('DELETE') !!}
                 <button type="submit" class="btn btn-danger">
                   <span class="glyphicon glyphicon-trash"></span>刪除
                 </button>
          </form>
      </div>
      <div class="col-sm-10 ">
        <img src="/photo/{{$photos->path}}" height="200">
        <form  action="{{asset('authrize/menu/photo/'.$photos->id)}}" method="post">
          {{ csrf_field() }}
          {{ method_field('PATCH') }}
          <h4>大標: </h4><input type="text" name="title" class="form-control" value="{{$photos->title}}">
          <h4>小標: </h4><input type="text" name="subtitle" class="form-control" value="{{$photos->subtitle}}">
          <h4>字體顏色: <input  value="{{$photos->color}}" name="color"class="jscolor form-control"><br><br>
          {{ csrf_field() }}
           <br><button type="submit" class="btn btn-success">確認修改</button>
        </form>
      </div>
</div><hr>


@endforeach



<script src="/js/news/jscolor.js"></script>
@endsection

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

@endsection

@section('js')
@endsection
