<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

use App\Models\ComReportForUser;
use App\Models\ComReportForProject;

class ComReportService
{
    private $url = "http://140.115.81.245:8080/";
    // private $url = "http://127.0.0.1:8080/";
    private $client;

    public function __construct(Client $client) {
        $this->client = $client;
    }

    public function findOne($comReportId) {
        $response = $this->client->get($this->url.'com-reports/'.$comReportId);
        $comReportJsonStr = $response->getBody()->getContents();
        $comReportJsonObj = json_decode($comReportJsonStr);
        $comReportForUser = $this->jsonObjToComReportForUser($comReportJsonObj);
        return $comReportForUser;
    }

    public function findByProjectId($projectId) {
        $response = $this->client->get($this->url.'projects/'.$projectId.'/com-reports');
        $projectComReportsJsonStr = $response->getBody()->getContents();
        $projectComReportsJsonObj = json_decode($projectComReportsJsonStr);
        $projectComReports = collect();
        foreach ($projectComReportsJsonObj as $projectComReportJsonObj) {
            $projectComReports->push($this->jsonObjToComReportForProject($projectComReportJsonObj));
        }
        return $projectComReports;
    }

    public function findByUserId($userId) {
        $response = $this->client->get($this->url.'users/'.$userId.'/com-reports');
        $userComReportsJsonStr = $response->getBody()->getContents();
        $userComReportsJsonObj = json_decode($userComReportsJsonStr);
        $userComReports = collect();
        foreach ($userComReportsJsonObj as $userComReportJsonObj) {
            $userComReports->push($this->jsonObjToComReportForUser($userComReportJsonObj));
        }
        return $userComReports;
    }

    public function save($userId, $projectKey, $comReport) {
        $comReportJsonStr = json_encode($comReport);
        $response = $this->client->post($this->url.'users/'.$userId.'/projects/'.$projectKey.'/com-reports', ['body' => $comReportJsonStr]);
        return $response->getBody()->getContents();
    }

    public function update($updateComReportId, $comReport) {
        $comReportJsonStr = json_encode($comReport);
        $response = $this->client->put($this->url.'com-reports/'.$updateComReportId, ['body' => $comReportJsonStr]);
    }

    public function delete($deleteComReportId) {
        $response = $this->client->delete($this->url.'com-reports/'.$deleteComReportId);
    }

    private function jsonObjToComReportForUser($comReportJsonObj) {
        $comReportForUser = new ComReportForUser();
        $comReportForUser->id = $comReportJsonObj->id;
        $comReportForUser->brandName = $comReportJsonObj->brandName;
        $comReportForUser->productName = $comReportJsonObj->productName;
        $comReportForUser->type = $comReportJsonObj->type;
        $comReportForUser->shape = $comReportJsonObj->shape;
        $comReportForUser->origin = $comReportJsonObj->origin;
        $comReportForUser->productionYear = $comReportJsonObj->productionYear;
        $comReportForUser->season = $comReportJsonObj->season;
        $comReportForUser->varieties = $comReportJsonObj->varieties;
        $comReportForUser->reportDate = $comReportJsonObj->reportDate;
        $comReportForUser->aromas = $comReportJsonObj->aromas;
        $comReportForUser->savors = $comReportJsonObj->savors;
        $comReportForUser->tastes = $comReportJsonObj->tastes;
        return $comReportForUser;
    }

    private function jsonObjToComReportForProject($comReportJsonObj) {
        $comReportForProject = new ComReportForProject();
        $comReportForProject->aromas = $comReportJsonObj->aromas;
        $comReportForProject->savors = $comReportJsonObj->savors;
        $comReportForProject->tastes = $comReportJsonObj->tastes;
        return $comReportForProject;
    }

}
