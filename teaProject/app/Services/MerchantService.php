<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

use App\Models\Merchant;

class MerchantService
{
    private $url = "http://140.115.81.245:8080/merchants/";
    // private $url = "http://127.0.0.1:8080/merchants/";
    private $client;

    public function __construct(Client $client) {
        $this->client = $client;
    }

    public function findAll() {
        $response = $this->client->get($this->url);
        $merchantsJsonStr = $response->getBody()->getContents();
        $merchantsJsonObj = json_decode($merchantsJsonStr);
        $merchants = collect();
        foreach ($merchantsJsonObj as $merchantJsonObj) {
            $merchants->push($this->jsonObjToMerchant($merchantJsonObj));
        }
        return $merchants;
    }

    public function findOne($merchantId) {
        $response = $this->client->get($this->url.$merchantId);
        $merchantJsonStr = $response->getBody()->getContents();
        $merchantJsonObj = json_decode($merchantJsonStr);
        $merchant = $this->jsonObjToMerchant($merchantJsonObj);
        return $merchant;
    }

    public function save($merchant) {
        $merchantJsonStr = json_encode($merchant);
        $response = $this->client->post($this->url, ['body' => $merchantJsonStr]);
    }

    public function update($updateMerchantId, $merchant) {
        $merchantJsonStr = json_encode($merchant);
        $response = $this->client->put($this->url.$updateMerchantId, ['body' => $merchantJsonStr]);
    }

    public function delete($deleteMerchantId) {
        $response = $this->client->delete($this->url.$deleteMerchantId);
    }

    private function jsonObjToMerchant($merchantJsonObj) {
        $merchant = new Merchant();
        $merchant->id = $merchantJsonObj->id;
        $merchant->brandName = $merchantJsonObj->brandName;
        $merchant->companyName = $merchantJsonObj->companyName;
        $merchant->guiNumber = $merchantJsonObj->guiNumber;
        $merchant->phone = $merchantJsonObj->phone;
        $merchant->contactPerson = $merchantJsonObj->contactPerson;
        $merchant->address = $merchantJsonObj->address;
        return $merchant;
    }

}
