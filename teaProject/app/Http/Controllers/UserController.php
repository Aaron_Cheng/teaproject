<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Services\UserService;

class UserController extends Controller
{
    private $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    public function getUsers() {
        $users = $this->userService->findAll();
        return view('user.usersInfo', ['users' => $users]);
    }

    public function getUser($userId) {
        $user = $this->userService->findOne($userId);
        return view('user.userInfo', ['user' => $user]);
    }

    public function getInfoForUser(Request $request) {
        $user = $request->session()->get('user');
        if($user !== null) {
            return view('user.userInfo', ['user' => $user]);
        } else {
            return view('login.unlogin');
        }
    }

    public function addUser(Request $request) {
        $googleUser = $request->session()->get('googleUser');
        $user = new User();
        $user->userName = $googleUser->name;
        $user->nationality = $request->input('nationality');
        $user->sex = $request->input('sex');
        $user->birth = $request->input('birth');
        $user->education = $request->input('education');
        $user->email = $googleUser->email;
        $user->phone = $request->input('phone');
        $user->residence = $request->input('residence');
        $user->profession = $request->input('profession');
        $user->frequency = $request->input('frequency');
        $user->hobbies = $request->input('hobbies');
        $user->preferenceFragrances = $request->input('preferenceFragrances');
        $user->preferenceTeaTypes = $request->input('preferenceTeaTypes');
        $user = $this->userService->save($user);
        if($user !== null) {
            $request->session()->put('user', $user);
            $request->session()->forget('googleUser');
        }
        return view('user.userInfo', ['user' => $user]);
    }

    public function returnUserUpdateForm(Request $request) {
        $user = $request->session()->get('user');
        return view('user.userUpdateForm', ['user' => $user]);
    }

    public function updateUser(Request $request) {
        $updateUserId = $request->session()->get('user')->id;
        $user = new User();
        $user->userName = $request->input('userName');
        $user->nationality = $request->input('nationality');
        $user->sex = $request->input('sex');
        $user->birth = $request->input('birth');
        $user->education = $request->input('education');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->residence = $request->input('residence');
        $user->profession = $request->input('profession');
        $user->frequency = $request->input('frequency');
        $user->hobbies = $request->input('hobbies');
        $user->preferenceFragrances = $request->input('preferenceFragrances');
        $user->preferenceTeaTypes = $request->input('preferenceTeaTypes');
        $updatedUser = $this->userService->update($updateUserId, $user);
        $request->session()->put('user', $updatedUser);
        return view('user.userInfo', ['user' => $updatedUser]);
    }

    public function deleteUser(Request $request) {
        $deleteUserId = $request->input('deleteUserId');
        $this->userService->delete($deleteUserId);
    }

    public function userLogout(Request $request) {
        $request->session()->forget('user');
        return view('homepage.homepage');
    }

}
