<?php

namespace App\Models;

class ProjectForMerchant
{
    public $id;
    public $productName;
    public $type;
    public $shape;
    public $origin;
    public $productionYear;
    public $season;
    public $varieties;
    public $reportDate;
}
