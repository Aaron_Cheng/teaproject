<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tea App</title>
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div class="flex-center position-ref full-height" align="center">
            <h1>Delete User</h1>
            <form action="/user-delete" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                Delete UserId: <input type="text" name="deleteUserId" /><br/><br/>
                <input type="submit" value="Delete" /><br/><br/>
            </form>
        </div>
    </body>
</html>
